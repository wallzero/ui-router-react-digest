/*
	eslint-disable
	canonical/filename-match-exported,
	unicorn/prevent-abbreviations
*/

import type {
	UIRouter
} from '@uirouter/core';
import {
	useRouter
} from '@uirouter/react';
import React, {
	useRef
} from 'react';
import type {
	ComponentType,
	FunctionComponent,
	ReactElement
} from 'react';

const connectWithRouter = <
	TRouterProps extends object,
	TNeedsProps extends object
>(
	params: (
		router: UIRouter,
		ownProps?: TNeedsProps
	) => TRouterProps = (
		router,
		ownProps
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	): any => {
		return {
			...ownProps,
			router
		};
	},
	update: boolean |
	((parameters: TRouterProps) => (
		Partial<TRouterProps>
	)) = false
): <P extends TRouterProps>(
	WrappedComponent: ComponentType<P>
) => FunctionComponent<Omit<P, keyof TRouterProps> & TNeedsProps> => {
	return <P extends TRouterProps>(
		WrappedComponent: ComponentType<P>
	): FunctionComponent<Omit<P, keyof TRouterProps> & TNeedsProps> => {
		const ConnectedWithRouter: FunctionComponent<Omit<P, keyof TRouterProps> & TNeedsProps> = (
			props: TNeedsProps
		): ReactElement => {
			const routerInstance = useRouter();
			const partialProps = useRef<Partial<TRouterProps> | undefined>(
				params(
					routerInstance,
					props
				)
			);

			if (update === true) {
				partialProps.current = params(
					routerInstance,
					props
				);
			} else if (update) {
				partialProps.current = {
					...partialProps.current,
					...update(
						params(
							routerInstance,
							props
						)
					)
				};
			}

			return (
				<WrappedComponent
					{...partialProps.current as P}
				/>
			);
		};

		return ConnectedWithRouter;
	};
};

export default connectWithRouter;

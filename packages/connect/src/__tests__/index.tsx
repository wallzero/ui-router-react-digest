import {
	render
} from '@testing-library/react';
import {
	memoryLocationPlugin,
	servicesPlugin,
	UIRouter,
	UIRouterReact
} from '@uirouter/react';
import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import connectWithRouter from '..';

type RouteProps = {
	title: string;
};

const Route: FunctionComponent = (): ReactElement => {
	return <div />;
};

describe(
	'UIRouter React Connect HOC',
	(): void => {
		// eslint-disable-next-line jest/expect-expect
		it(
			'Connect with default properties',
			(): void => {
				const Connect = connectWithRouter()(Route);
				render(
					<UIRouter
						plugins={[memoryLocationPlugin]}
					>
						<Connect />
					</UIRouter>
				);
			}
		);

		it(
			'passed router matches',
			(): void => {
				const router = new UIRouterReact();
				router.plugin(servicesPlugin);
				router.plugin(memoryLocationPlugin);

				const mapRouterToProps = (
					_router: UIRouterReact
				): Record<string, unknown> => {
					expect(_router).toBe(router);

					return {};
				};

				const Connect = connectWithRouter(
					// eslint-disable-next-line @typescript-eslint/ban-ts-comment
					// @ts-expect-error
					mapRouterToProps,
					false
				)(Route);

				render(
					<UIRouter
						router={router}
					>
						<Connect />
					</UIRouter>
				);
			}
		);

		// eslint-disable-next-line jest/expect-expect
		it(
			'Connect without update',
			(): void => {
				// eslint-disable-next-line unicorn/consistent-function-scoping
				const mapRouterToProps = (): Record<string, unknown> => {
					return {};
				};

				const Connect = connectWithRouter(
					mapRouterToProps,
					false
				)(Route);
				render(
					<UIRouter
						plugins={[memoryLocationPlugin]}
					>
						<Connect />
					</UIRouter>
				);
			}
		);

		// eslint-disable-next-line jest/expect-expect
		it(
			'Connect with default update',
			(): void => {
				// eslint-disable-next-line unicorn/consistent-function-scoping
				const mapRouterToProps = (): Record<string, unknown> => {
					return {};
				};

				const Connect = connectWithRouter(
					mapRouterToProps,
					true
				)(Route);
				render(
					<UIRouter
						plugins={[memoryLocationPlugin]}
					>
						<Connect />
					</UIRouter>
				);
			}
		);

		// eslint-disable-next-line jest/expect-expect
		it(
			'Connect with custom update',
			(): void => {
				// eslint-disable-next-line unicorn/consistent-function-scoping
				const mapRouterToProps = (): Record<string, unknown> => {
					return {};
				};

				const Connect = connectWithRouter(
					mapRouterToProps,
					(props): Partial<RouteProps> => {
						return props;
					}
				)(Route);
				render(
					<UIRouter
						plugins={[memoryLocationPlugin]}
					>
						<Connect />
					</UIRouter>
				);
			}
		);

		/*
			Having difficulty triggering router to update
		*/
		// eslint-disable-next-line jest/expect-expect,jest/prefer-expect-assertions
		it(
			'update on transition',
			async (): Promise<void> => {
				const states = [
					{
						component: Route,
						name: 'home'
					},
					{
						component: (): ReactElement => {
							return <div />;
						},
						name: 'test'
					}
				];
				const router = new UIRouterReact();
				router.plugin(servicesPlugin);
				router.plugin(memoryLocationPlugin);
				router.stateRegistry.register(states[0]);
				router.stateRegistry.register(states[1]);
				// eslint-disable-next-line unicorn/consistent-function-scoping
				const mapRouterToProps = (): Record<string, unknown> => {
					return {};
				};

				const Connect = connectWithRouter(
					mapRouterToProps,
					(props): Partial<RouteProps> => {
						return props;
					}
				)(Route);

				// eslint-disable-next-line @typescript-eslint/no-extra-parens
				const JSX = (
					<UIRouter
						router={router}
					>
						<Connect />
					</UIRouter>
				);

				const component = render(JSX);
				await router.stateService.go('home');
				component.rerender(JSX);
				await router.stateService.go('test');
				component.rerender(JSX);
			}
		);
	}
);

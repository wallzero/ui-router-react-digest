/*
	eslint-disable
	filenames/match-regex,
	filenames/match-exported,
	import/unambiguous,
	import/no-commonjs,
	@typescript-eslint/no-var-requires
*/

// eslint-disable-next-line @typescript-eslint/no-require-imports
const jest = require('@digest/jest');

jest.testEnvironment = 'jsdom';

module.exports = jest;

<div align="center">
	<h1>UIRouter React Connect</h1>
</div>

<div align="center">
	<a href="https://commitizen.github.io/cz-cli/"><img alt="Commitizen Friendly" src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg" /></a>
	<a href="https://gitlab.com/hyper-expanse/semantic-release-gitlab#readme"><img alt="Semantic Release" src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg" /></a>
</div>

<div align="center">
	<a href="https://gitlab.com/wallzero/ui-router-react-digest/commits/master"><img alt="Build Status" src="https://gitlab.com/wallzero/ui-router-react-digest/badges/master/pipeline.svg" /></a>
	<a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-connect/coverage/lcov-report/"><img alt="Coverage Report" src="https://gitlab.com/wallzero/ui-router-react-digest/badges/master/coverage.svg" /></a>
	<a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-connect/webpack/"><img alt="Build Analysis" src="https://img.shields.io/badge/webpack-stats-blue.svg" /></a>
	<a href="https://www.npmjs.org/package/ui-router-react-digest"><img alt="NPM Version" src="https://img.shields.io/npm/v/ui-router-react-digest.svg" /></a>
	<a href="https://www.gnu.org/licenses/gpl-3.0.en.html"><img alt="License" src="https://img.shields.io/npm/l/ui-router-react-digest.svg" /></a>
	<a href="https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-connect?ref=badge_shield"><img alt="FOSSA Status" src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-connect.svg?type=shield" /></a>
	<a href="https://github.com/gajus/canonical"><img alt="Canonical Code Style" src="https://img.shields.io/badge/code%20style-canonical-blue.svg" /></a>
</div>

**UIRouter React Connect** is a high-order component that allows parsing and passing of `router` values into components.

## Installation

`npm install ui-router-react-connect`

### Using `connectWithRouter`

This can be useful when a component requires access to current router
state information. For example:

```tsx
/* typescript */

import {UIRouterReact} from '@uirouter/react';
import {connect} from 'react-redux';
import connectWithRouter from 'ui-router-react-connect';
import Component from './Component';

const mapRouterToProps = (router: UIRouterReact) => ({
	id: router.stateService.params.id,
	index: router.stateService.params.index
});

/* After component loads, only update index on route changes */
const update = ({index}: {index: number}) => ({
	index
})

const mapStateToProps = (
	state,
	{id, index}: {id: number, index: number}
) => ({
	index,
	zoom: state.tab[id].zoom
});

export default connectWithRouter(mapRouterToProps, update)(
	connect(mapStateToProps)(Component)
);
```


#### `connectWithRouter` Function

| Parameters | Type | Default | Description |
|---|---|---|---|
| **mapRouterToProps** | function | `(router, ownProps) => ({...ownProps, router});` | Pass a function that recieves the router instance and ownProps (any additional external props passed into the component) and returns an object of props that will be passed along to the component |
| *defaultUpdate* | function&#124;`true`&#124;<code>**false**</code> | if `true`: `(params) => (params)` | Allows control over what props are updated on state changes. Passing `false` (the default) will not update the component props after inception. This is useful when a component should reflect the route state from when it was initialized, not current routes. |

<div align="center">
	<h1>UIRouter React Digest</h1>
</div>

<div align="center">
	<a href="https://commitizen.github.io/cz-cli/"><img alt="Commitizen Friendly" src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg" /></a>
	<a href="https://github.com/semantic-release/semantic-release#readme"><img alt="Semantic Release" src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg" /></a>
</div>

<div align="center">
	<a href="https://gitlab.com/wallzero/ui-router-react-digest/commits/master"><img alt="Build Status" src="https://gitlab.com/wallzero/ui-router-react-digest/badges/master/pipeline.svg" /></a>
	<a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-digest/coverage/lcov-report/"><img alt="Coverage Report" src="https://gitlab.com/wallzero/ui-router-react-digest/badges/master/coverage.svg" /></a>
	<a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-digest/webpack/"><img alt="Build Analysis" src="https://img.shields.io/badge/webpack-stats-blue.svg" /></a>
	<a href="https://www.npmjs.org/package/ui-router-react-digest"><img alt="NPM Version" src="https://img.shields.io/npm/v/ui-router-react-digest" /></a>
	<a href="https://www.gnu.org/licenses/gpl-3.0.en.html"><img alt="License" src="https://img.shields.io/npm/l/ui-router-react-digest.svg" /></a>
	<a href="https://app.fossa.io/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-digest?ref=badge_shield"><img alt="FOSSA Status" src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fwallzero%2Fui-router-react-digest.svg?type=shield" /></a>
	<a href="https://github.com/gajus/canonical"><img alt="Canonical Code Style" src="https://img.shields.io/badge/code%20style-canonical-blue.svg" /></a>
</div>

<h5> </h5>

<div align="center">
	<a href="https://gitlab.com/wallzero/ui-router-react-digest/raw/76114cde8c904c6675d7aef7364724cb828a4b39/static/demo_2018-01-24.gif">
		<img src="https://gitlab.com/wallzero/ui-router-react-digest/raw/76114cde8c904c6675d7aef7364724cb828a4b39/static/demo_2018-01-24.gif" alt="usage" style="max-width:100%;">
	</a>
</div>

<h2> </h2>

**UIRouter React Digest** renders a common UI layout while prepopulating UIRouter with the appropriate states - greatly reducing the boilerplate needed for a common UI layout and routing strategy while still enabling rich customization.

## Installation

First install the peer dependencies `react`, `react-dom`, `@uirouter/react`:

`npm install react react-dom @uirouter/react`

Then install `ui-router-react-digest`:

`npm install ui-router-react-digest`

## Demo

| <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-digest/coverage/lcov-report/"><h4>Code coverage</h4></a> | Demo | Stats | Source |
| --- | ---| --- | --- | --- |
| Simple Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/simple/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/simple/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/simple">link</a> |
| Redux Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/redux/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/redux/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/redux">link</a> |
| Material Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/material/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/material/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/material">link</a> |

## Simple example

```tsx
/* typescript */

import {DSRPlugin} from '@uirouter/dsr';
import {
	hashLocationPlugin,

	// pushStateLocationPlugin,
	servicesPlugin,
	UIRouterReact
} from '@uirouter/react';
import {StickyStatesPlugin} from '@uirouter/sticky-states';
import React, {
	CSSProperties,
	ReactElement,
	SFC,
	useRef,
	useState
} from 'react';
import {
	Drawer,
	Drawers,
	generateStateName,
	Orientation,
	Tab,
	Tabs,
	UIRouterReactDigest
} from 'ui-router-react-digest';
import 'ui-router-react-digest/dist/main.min.css';

// import {Visualizer} from '@uirouter/visualizer';

const style: CSSProperties = {
	/* stylelint-disable */
	minWidth: 280,
	padding: 20
};

const drawerStyle: CSSProperties = {
	backgroundColor: '#fff',
	padding: 20
};

const errorStyle: CSSProperties = {
	border: '20px solid black',
	borderImage:
	// eslint-disable-next-line no-multi-str
	'repeating-linear-gradient( \
		45deg, \
		black, \
		black 2px, \
		#fff 2px, \
		#fff 14px, \
		black 14px, \
		black 16px, \
		#fff 16px, \
		#fff 28px \
	) 20 20',
	height: 'calc(100% - 80px)',
	minWidth: 280,
	padding: 20
};

const rootState = {
	name: 'root',
	params: {
		lang: {
			dynamic: true,
			value: 'en'
		}
	},
	url: '/:lang'
};

const App: SFC = (): ReactElement => {
	const router = useRef<UIRouterReact>(null);

	if (!router.current) {
		router.current = new UIRouterReact();

		router.current.plugin(servicesPlugin);
		router.current.plugin(hashLocationPlugin);

		// router.current.plugin(pushStateLocationPlugin);
		router.current.plugin(DSRPlugin);
		router.current.plugin(StickyStatesPlugin);

		// router.current.plugin(Visualizer);

		router.current.urlRouter.when('', () => {
			router.current.stateService.go(
				generateStateName(
					rootState.name,
					'home'
				),
				{},
				{location: 'replace'}
			);
		});

		router.current.urlRouter.when('/', () => {
			router.current.stateService.go(
				generateStateName(
					rootState.name,
					'home'
				),
				{},
				{location: 'replace'}
			);
		});

		// In case of no route found, go to first tab
		router.current.urlRouter.otherwise(() => {
			router.current.stateService.go(
				generateStateName(
					rootState.name,
					'404'
				),
				{},
				{
					location: false,
					reload: true
				}
			);
		});

		router.current.transitionService.onSuccess(
			{
				to: generateStateName(
					rootState.name,
					'**'
				)
			},
			(transition) => {
				document.title = transition.targetState().state().params.title;
			}
		);
	}

	const [
		drawerDocked
	] = useState(true);

	const [
		drawerDrag
	] = useState(false);

	const [
		drawerHover
	] = useState(true);

	const [
		drawerIndex,
		setDrawerIndex
	] = useState(0);

	const [
		drawerOpen,
		setDrawerOpen
	] = useState(true);

	// const [
	//   title
	// ] = useState('UIRouter React Digest');

	const [
		orientation
	] = useState('left' as Orientation);

	const [
		tabIndex,
		setTabIndex
	] = useState(0);

	const handleDrawerOpen = (): void => {
		setDrawerOpen(!drawerOpen);
	};

	return (
		<UIRouterReactDigest
			drawerDocked={drawerDocked}
			drawerDrag={drawerDrag}
			drawerHover={drawerHover}
			drawerIndex={drawerIndex}
			drawerOpen={drawerOpen}
			onDrawerOpenToggle={handleDrawerOpen}
			onDrawerSelect={setDrawerIndex}
			onTabSelect={setTabIndex}
			orientation={orientation}
			rootState={rootState}
			router={router.current}
			tabIndex={tabIndex}
		>
			<Drawers>
				<Drawer
					name='tools'
					state={{
						params: {
							title: 'Tools'
						}
					}}
				/>
				<Drawer
					name='settings'
					state={{
						params: {
							title: 'Settings'
						}
					}}
				>
					<div style={drawerStyle}>
						<h4>
							Settings
						</h4>
					</div>
				</Drawer>
			</Drawers>
			<Tabs>
				<Tab
					drawers={{
						tools: () => {
							return (
								<div style={drawerStyle}>
									<h4>
										404
									</h4>
								</div>
							);
						}
					}}
					hidden={true}
					name='404'
					state={{
						params: {
							title: '404'
						},
						sticky: true
					}}
				>
					<div style={errorStyle}>
						<h4>
							404
						</h4>
					</div>
				</Tab>
				<Tab
					drawers={{
						tools: () => {
							return (
								<div style={drawerStyle}>
									<h4>
										Home Tools
									</h4>
								</div>
							);
						}
					}}
					name='home'
					state={{
						params: {
							title: 'Home'
						}
					}}
				>
					<div style={style}>
						<h4>
							Home
						</h4>
					</div>
				</Tab>
				<Tab
					drawers={{
						tools: () => {
							return (
								<div style={drawerStyle}>
									<h4>
										Content Tools
									</h4>
									<div style={{height: 1000}} />
								</div>
							);
						}
					}}
					name='content'
					state={{
						params: {
							title: 'Content'
						}
					}}
				>
					<div style={drawerStyle}>
						<h4>
							Content
						</h4>
						<div style={{height: 1000}} />
					</div>
				</Tab>
			</Tabs>
		</UIRouterReactDigest>
	);
};

export default App;

```

## API

#### `UIRouterReactDigest` Component Props

| Name | Type | Default | Description |
|---|---|---|---|
| drawerDocked | boolean | `true` | Whether the drawer will close when outside content is clicked. |
| drawerDrag | boolean | `false` | Whether the drawer can be dragged open via touch events. |
| drawerFooter | component | included `drawer footer` component | Allows customization of the drawers footer. No props are passed. |
| drawerHeader | component | included `drawer header` component.  | Allows customization of the drawers header. `drawerIndex`, `drawers`, and `onDrawerSelect` props are passed along into this component. |
| drawerHover | boolean | `false` | Whether the drawer hovers over content or pushes it over. |
| drawerIndex | number | `0` | The selected drawer tab. |
| drawerOpen | boolean | `false` | Drawer open state. |
| drawerWidth | number &#124; string | `300` | The width of the drawer. |
| footer | component | included `footer` component | A footer applied to each tab. |
| header | component | included `header` component | The page header. `drawerDocked`, `drawerOpen`, `onDrawerOpenToggle`, `onTabSelect`, `orientation`, `tabIndex`, `tabs` and `title` props are passed along into this component. |
| onDrawerOpenToggle | function (open: boolean) |  | Callback prop when the drawer is opened or closed |
| onDrawerSelect | function (index: number) |  | Callback prop when a drawer tab is selected. |
| onTabSelect | function (index: number) |  | Callback prop when a page tab is selected. |
| orientation | string ('left' or 'right') | `left` | Whether the drawer is aligned to the left or right of the page. |
| rootState | object | `{name: 'uurd'}` | A state which is prepended to all initial routes. Useful if the path needs to contain additional properties such as language. |
| router | UIRouter instance | A UIRouter instance with servicesPlugin and pushStateLocationPlugin configured | A custom UIRouter instance can be passed for additional configuration such as plugins. |
| tabIndex | number | `0` | The current tab index. A change in this index will reactively change the route. |
| title | string | `Digest` | The title displayed in the default `header` component. Not neccessary if you pass a custom header component. |

#### `tab` Component Props

| Name | Type | Default | Description |
|---|---|---|---|
| drawers | object `{name: component}` | `{}` | Apply a tab specific component to context drawers. Drawer must have a corresponding `Drawer` component and the names must match. |
| footer | component | | A footer applied to this tab. Overrides default footer. |
| hidden | boolean | false | Whether this tab should be hidden. Hidden tabs aren't swipeable and are not listed in the provided default header tabs. Useful for navigation pages such as `404`. |
| name | string |  | The unique identifier of this tab. |
| state | object | `{}` | `@uirouter/react` state definition. Any property normally applied to a state definition, such as `abstract`, `sticky`, `deepStateRedirect`, etc. will work here with the exception of `component`, `url`, and `view`. | 
| swipe | boolean | true | Whether switching tabs by swiping left or right is enabled on this tab. |

#### `drawer` Component Props

***Note:*** *drawers are not dynamic and can't be changed after `ui-router-react-digest` is mounted.*

| Name | Type | Default | Description |
|---|---|---|---|
| name | string |  | The unique identifier of this drawer. |
| state | object | `{}` | This is provided to pass additional parameters via `state.params`. Currently no state properties are used. |

If children are not provided to the drawer component, the drawer is considered contextual and the component is provided by the `drawers` property in the `Tab` component.

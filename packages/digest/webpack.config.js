/*
	eslint-disable
	import/unambiguous,
	import/no-commonjs,
	filenames/match-exported,
	filenames/match-regex,
	@typescript-eslint/no-var-requires
*/
const path = require('path');
const webpackDigest = require('@digest/webpack');
const nodeExternals = require('webpack-node-externals');

webpackDigest.resolve.alias = {
	'@babel/runtime': path.resolve(__dirname, '..', '..', 'node_modules', '@babel', 'runtime')
};

webpackDigest.externals = [
	nodeExternals({modulesDir: path.resolve(__dirname, 'node_modules')}),
	nodeExternals({modulesDir: path.resolve(__dirname, '..', '..', 'node_modules')})
];

module.exports = webpackDigest;

const generateStateName = (
	shortName = 'urrd',
	tabName: number | string = 'undefined'
): string => {
	return `${shortName}.tabs.${tabName}`;
};

export default generateStateName;

import type {
	Resolvable,
	StateObject,
	StateParams,
	Transition
} from '@uirouter/react';
import React, {
	useMemo
} from 'react';
import type {
	ElementType,
	FunctionComponent,
	ReactElement
} from 'react';

/*
	UIView passes uneccesary props to the immediate child which causes a rerender.
	This fixes this for now.
*/
export default function<T extends {
	$state$: StateObject;
	$stateParams: StateParams;
	$transition$: Transition;
	resolves: Resolvable;
	transition: Transition;
}> (
	Component: ElementType
): FunctionComponent<T> {
	return ({
		...filter
	}: T): ReactElement => {
		const component = useMemo(
			(): ReactElement => {
				return <Component {...filter} />;
			},
			[ // eslint-disable-line react-hooks/exhaustive-deps
				// eslint-disable-next-line react-hooks/exhaustive-deps
				...Object.keys(filter).map(
					(key): keyof T => {
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						return filter[key];
					}
				)
			]
		);

		return component;
	};
}

import type {
	ReactStateDeclaration,
	ReactViewDeclaration
} from '@uirouter/react/lib/interface';
import {
	cloneElement
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ElementType
} from 'react';
import type {
	DrawerProps
} from 'src';
import Missing from 'src/components/_defaults/Missing';

const generateStateName = (
	shortName: string,
	tabName: number | string
): string => {
	return `${shortName}.tabs.${tabName}`;
};

const initTabViews = (
	name: string,
	shortName: string,
	tab: ReactElement,
	footer: ElementType,
	drawers: {[key: string]: ElementType} | undefined,
	drawerMap: Array<ReactElement<DrawerProps>>,
	state: ReactStateDeclaration | undefined
): ReactStateDeclaration => {
	const views: {
		[key: string]: ReactViewDeclaration;
	} = {
		[name]: {
			component: tab ?
				(props): ReactElement => {
					return cloneElement(
						tab,
						props
					);
				} :
				Missing
		}
	};

	if (footer) {
		views.footer = {component: footer as FunctionComponent};
	}

	for (const drawer of drawerMap) {
		if (drawer.props.children === undefined) {
			const viewName = `${name}_${drawer.props.name}@${shortName}`;

			views[viewName] = {
				component: drawers?.[drawer.props.name] ?
					drawers[drawer.props.name] as FunctionComponent :
					Missing
			};
		}
	}

	return {
		...state,
		name: generateStateName(
			shortName,
			name
		),
		params: {
			...state ?
				state.params :
				{},
			id: {
				squash: true,
				value: name
			}
		},
		url: `/${name}`,
		views
	};
};

export default initTabViews;

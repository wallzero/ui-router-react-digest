import {
	pushStateLocationPlugin,
	servicesPlugin,
	UIRouter,
	UIRouterReact,
	UIView
} from '@uirouter/react';
import type {
	Transition
} from '@uirouter/react';
import type {
	ReactStateDeclaration,
	ReactViewDeclaration
} from '@uirouter/react/lib/interface';
import React, {
	Children,
	cloneElement,
	useEffect,
	useMemo,
	useRef,
	useState
} from 'react';
import type {
	FunctionComponent,
	KeyboardEvent,
	ReactElement,
	ReactNode,
	ReactText,
	ElementType,
	SyntheticEvent
} from 'react';
import AppView from 'src/components/AppView';
import type {
	AppViewProps
} from 'src/components/AppView';
import TempDrawer from 'src/components/Drawer';
import TempDrawers from 'src/components/Drawers';
import TempTab from 'src/components/Tab';
import TabView from 'src/components/TabView';
import TempTabs from 'src/components/Tabs';
import DrawerFooter from 'src/components/_defaults/DrawerFooter';
import DrawerHeader from 'src/components/_defaults/DrawerHeader';
import Footer from 'src/components/_defaults/Footer';
import Header from 'src/components/_defaults/Header';
import Missing from 'src/components/_defaults/Missing';
import defaults from 'src/defaults';
import generateStateNameTemporary from 'src/util/generateStateName';
import initTabViews from 'src/util/initTabViews';
import './index.css';
import './styles';

const Drawer = TempDrawer;
const Drawers = TempDrawers;
const generateStateName = generateStateNameTemporary;
const Tab: FunctionComponent<TabProps> = TempTab;
const Tabs = TempTabs;

const defaultRules = (
	router: UIRouterReact,
	path: string,
	rootState: ReactStateDeclaration,
	tabs: Array<ReactElement<TabProps>>,
	tabIndex: number
): void => {
	if (
		!router.urlRouter.rules().some(
			(rule): boolean => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-expect-error
				return rule.urlMatcher && rule.urlMatcher.pattern === path;
			}
		)
	) {
		router.urlService.rules.when(
			path,
			(): void => {
				// Don't load hidden tabs when they were the last ones visited; i.e 404 page
				const index = tabs[tabIndex].props.hidden ?
					tabs.findIndex(
						(tab): boolean => {
							return !tab.props.hidden;
						}
					) :
					tabIndex;

				void router.stateService.go(
					generateStateName(
						rootState.name,
						tabs[index].props.name
					),
					{},
					{
						location: 'replace'
					}
				);
			}
		);
	}
};

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
const UIRouterReactDigest: FunctionComponent<UIRouterReactDigestProps> = (
	{
		children,
		drawerDocked = defaults.drawerDocked,
		drawerDrag = defaults.drawerDrag,
		drawerFooter = DrawerFooter,
		drawerHeader = DrawerHeader,
		drawerHover = defaults.drawerHover,
		drawerIndex = defaults.drawerIndex,
		drawerOpen = defaults.drawerOpen,
		drawerWidth = defaults.drawerWidth,
		footer = Footer,
		header = Header,
		onDrawerOpenToggle = defaults.onDrawerOpenToggle,
		onDrawerSelect = defaults.onDrawerSelect,
		onTabSelect = defaults.onTabSelect,
		orientation = defaults.orientation,
		rootState = defaults.rootState,
		// eslint-disable-next-line unicorn/prevent-abbreviations
		router: propRouter,
		tabIndex
	}: Required<UIRouterReactDigestProps>
): ReactElement | null => {
	const router = useRef<UIRouterReact>();
	const drawers = useRef<Array<ReactElement<DrawerProps>>>([]);
	const tabs = useRef<Array<ReactElement<TabProps>>>([]);
	const tabIndexRef = useRef<number>(tabIndex);

	tabIndexRef.current = tabIndex;

	const [
		// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
		// @ts-ignore
		_init, // eslint-disable-line @typescript-eslint/no-unused-vars,@typescript-eslint/naming-convention
		setInit
	] = useState(false);

	tabs.current = Children.toArray(
		(
			Children.toArray(children as ReactElement).find(
				(child): boolean => {
					// eslint-disable-next-line @typescript-eslint/ban-ts-comment
					// @ts-expect-error
					return child.type.displayName === 'Tabs';
				}
			) as ReactElement
		).props.children
	).filter(
		(child): boolean => {
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			return child.type.displayName === 'Tab';
		}
	) as Array<ReactElement<TabProps>> || [];

	useEffect(
		(): void => {
			if (
				router.current?.started &&
				tabs.current &&
				tabs.current[tabIndex] &&
				!(tabs.current[tabIndex] &&
					router.current.stateService.current.name?.includes(
						generateStateName(
							rootState.name,
							tabs.current[tabIndex].props.name
						)
					)
				)
			) {
				void router.current.stateService.go(
					generateStateName(
						rootState.name,
						tabs.current[tabIndex].props.name
					)
				);
			}
		},
		[tabIndex] // eslint-disable-line react-hooks/exhaustive-deps
	);

	useEffect(
		(): void => {
			router.current = propRouter || new UIRouterReact();

			if (!propRouter) {
				router.current.plugin(servicesPlugin);
				router.current.plugin(pushStateLocationPlugin);
			}

			router.current.stateRegistry.register({
				...rootState,
				abstract: true,
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-expect-error
				component: AppView
			});

			const root: {[key: string]: ReactViewDeclaration} = {
				tabs: {component: TabView}
			};

			if (header) {
				root.header = {component: header as FunctionComponent};
			}

			if (drawerHeader) {
				root.drawerHeader = {component: drawerHeader as FunctionComponent};
			}

			if (drawerFooter) {
				root.footer = {component: drawerFooter as FunctionComponent};
			}

			drawers.current = Children.toArray(
				(
					Children.toArray(children as ReactElement<DrawerProps>).find(
						(child): boolean => {
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							return child.type.displayName === 'Drawers';
						}
					) as ReactElement<DrawerProps>
				).props.children
			).filter(
				(child): boolean => {
					return Boolean(
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						child.type && child.type.displayName === 'Drawer'
					);
				}
			) as Array<ReactElement<DrawerProps>> || [];

			for (const drawer of drawers.current) {
				root[drawer.props.name] = {
					component: drawer.props.children ?
						((): ReactNode => {
							return drawer.props.children;
						}) as FunctionComponent :
						Missing as FunctionComponent
				};
			}

			router.current.stateRegistry.register({
				abstract: true,
				name: `${rootState.name ?? 'root'}.tabs`,
				views: root
			});

			for (const tab of tabs.current) {
				// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
				router.current!.stateRegistry.register(
					initTabViews(
						tab.props.name,
						rootState.name ?? 'root',
						tab.props.children as ReactElement,
						footer,
						tab.props.drawers,
						drawers.current,
						tab.props.state
					)
				);
			}

			defaultRules(
				router.current,
				'',
				rootState,
				tabs.current,
				tabIndex
			);

			defaultRules(
				router.current,
				'/',
				rootState,
				tabs.current,
				tabIndex
			);

			// Resolve current tab on load and back button
			router.current.transitionService.onSuccess(
				{to: `${rootState.name ?? 'root'}.tabs.**`},
				(transition: Transition): void => {
					const {name} = transition.targetState().state();
					const id = name?.split(/\./u)[2];
					tabs.current.some((
						tab,
						index
					): boolean => {
						if (tab.props.name === id) {
							if (tabIndexRef.current !== index) {
								onTabSelect(index);
							}

							return true;
						}

						return false;
					});
				}
			);

			setInit(true);
		},
		[] // eslint-disable-line react-hooks/exhaustive-deps
	);

	const app = useMemo(
		(): ((App: ElementType<AppViewProps>) => ReactElement) => {
			return (App: ElementType<AppViewProps>): ReactElement => {
				const id = tabs.current[tabIndexRef.current] ?
					`urrd_${tabs.current[tabIndexRef.current].props.name}` :
					false;

				const skipToContent = (
					event: KeyboardEvent<HTMLAnchorElement> | SyntheticEvent<HTMLAnchorElement>
				): void => {
					event.preventDefault();

					if (id) {
						const skipContent = document.querySelector(`#${id}`);

						if (skipContent) {
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							skipContent.focus();
						}
					}
				};

				const onKeyPress = (
					event: KeyboardEvent<HTMLAnchorElement>
				): void => {
					switch (event.charCode) {
						case 0:
						case 13:
						case 32:
							skipToContent(event);
							break;
						default:
							break;
					}
				};

				return (
					<>
						<a
							className='urrd-skip-link'
							href={`#${id}`}
							id='skip-link'
							onClick={skipToContent}
							onKeyPress={onKeyPress}
						>
							Skip to main content
						</a>
						<App
							drawerDocked={drawerDocked}
							drawerDrag={drawerDrag}
							drawerHover={drawerHover}
							drawerIndex={drawerIndex}
							drawerOpen={drawerOpen}
							drawerWidth={drawerWidth}
							onDrawerOpenToggle={onDrawerOpenToggle}
							onDrawerSelect={onDrawerSelect}
							onTabSelect={onTabSelect}
							orientation={orientation}
							tabIndex={tabIndexRef.current}
						>
							{{
								drawers: drawers.current,
								tabs: router.current ?
									(tabs.current as Array<ReactElement<TabFullProps>>).map((
										tab: ReactElement<TabFullProps>,
										index
									): ReactElement => {
										return cloneElement(
											tab,
											{
												drawerMap: drawers.current,
												footer: tab.props.footer ?? footer,
												index,
												onTabSelect,
												router: router.current,
												shortName: rootState.name,
												tabIndex: tabIndexRef.current,
												tabs: tabs.current
											}
										);
									}) :
									undefined
							}}
						</App>
					</>
				);
			};
		},
		[ // eslint-disable-line react-hooks/exhaustive-deps
			tabs.current,
			tabIndexRef.current,
			drawerDocked,
			drawerDrag,
			drawerHover,
			drawerIndex,
			drawerOpen,
			drawerWidth,
			onDrawerOpenToggle,
			onDrawerSelect,
			onTabSelect,
			orientation,
			tabIndex,
			drawers.current,
			router.current,
			rootState.name
		]
	);

	return (
		router.current ?
			// eslint-disable-next-line @typescript-eslint/no-extra-parens
			(
				<UIRouter router={router.current}>
					<UIView render={app} />
				</UIRouter>
			) :
			null
	);
};

export type Orientation = 'left' | 'right';

export type UIRouterReactDigestProps = {
	children: ReactNode;
	drawerDocked?: boolean;
	drawerDrag?: boolean;
	drawerFooter?: ElementType;
	drawerHeader?: ElementType;
	drawerHover?: boolean;
	drawerIndex?: number;
	drawerOpen?: boolean;
	drawerWidth?: ReactText;
	footer?: ElementType;
	header?: ElementType;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onDrawerOpenToggle?: (bool?: boolean) => any;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onDrawerSelect?: (index: number) => any;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onTabSelect?: (index: number) => any;
	orientation?: Orientation;
	rootState?: ReactStateDeclaration;
	router?: UIRouterReact;
	tabIndex?: number;
};

export type DrawerProps = {
	children?: ReactNode;
	name: string;
	state?: ReactStateDeclaration;
};

export type DrawersProps = {
	children: ReactNode;
};

export type TabFullProps = TabProps & {
	drawerMap?: Array<ReactElement<DrawerProps>>;
	index?: number;
	onTabSelect?: (index: number) => void;
	router?: UIRouterReact;
	shortName?: string;
	tabIndex?: number;
	tabs?: Array<ReactElement<TabProps>>;
	view?: ReactNode;
};

export type TabProps = {
	children?: ReactNode;

	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	drawers?: {[key: string]: any};
	footer?: ElementType;
	hidden?: boolean;
	name: string;
	state?: ReactStateDeclaration;
	swipe?: boolean;
};

export type TabsProps = {
	children: ReactNode;
};

export {
	Drawer,
	Drawers,
	generateStateName,
	Tab,
	Tabs,
	UIRouterReactDigest
};

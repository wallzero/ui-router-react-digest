import React, {
	memo,
	useMemo,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import SwipeableViews from 'react-swipeable-views';
import type {
	TabProps
} from 'src';
import Content from 'src/components/Content';
import type {
	Orientation
} from 'src/index';
import './DrawerContext.css';

const DrawerContext: FunctionComponent<DrawerContextProps> = (
	{
		children,
		className,
		currentDrawer,
		drawer,
		drawerIndex,
		orientation,
		tabIndex
	}: DrawerContextProps
): ReactElement | null => {
	const [
		stateTabIndex,
		setStateTabIndex
	] = useState(0);
	useMemo(
		(): void => {
			setStateTabIndex(tabIndex);
		},
		[tabIndex]
	);

	const isCurrentDrawer = currentDrawer === drawerIndex;

	const drawerTabs = useMemo(
		(): ReactElement[] => {
			return children.map(
				(
					tab,
					index
				): ReactElement => {
					const isCurrentTab = index === tabIndex;

					return (
						<div
							className='urrd-drawer'
							key={tab.props.name}
						>
							<Content
								className={className}
								focus={isCurrentDrawer && isCurrentTab}
								name={`${tab.props.name}_${drawer.props.name}`}
								orientation={orientation}
								role='complementary'
							/>
						</div>
					);
				}
			);
		},
		[
			children,
			tabIndex,
			className,
			isCurrentDrawer,
			drawer.props.name,
			orientation
		]
	);

	const content = useMemo(
		(): ReactElement | null => {
			return drawer.props.children ?
				// eslint-disable-next-line @typescript-eslint/no-extra-parens
				(
					<Content
						className={className}
						focus={isCurrentDrawer}
						name={drawer.props.name}
						orientation={orientation}
						role='complementary'
					/>
				) :
				null;
		},
		[
			className,
			isCurrentDrawer,
			drawer.props.name,
			drawer.props.children,
			orientation
		]
	);

	const views = useMemo(
		(): ReactElement | null => {
			return drawer.props.children ?
				null :
				// eslint-disable-next-line @typescript-eslint/no-extra-parens
				(
					<SwipeableViews
						className='urrd-drawer-swipe'
						disableLazyLoading
						disabled
						index={
							children[stateTabIndex] ?
								stateTabIndex :
								0
						}
					>
						{drawerTabs}
					</SwipeableViews>
				);
		},
		[ // eslint-disable-line react-hooks/exhaustive-deps
			children,
			stateTabIndex,
			drawerTabs
		]
	);

	return (
		drawer.props.children ?
			content :
			views
	);
};

export default memo(DrawerContext);

export type DrawerContextProps = {
	children: Array<ReactElement<TabProps>>;
	className: string;
	currentDrawer: number;
	drawer: ReactElement;
	drawerIndex: number;
	orientation: Orientation;
	tabIndex: number;
};

export type DrawerContextState = {
	tabIndex: number;
};

import {
	UIView
} from '@uirouter/react';
import React, {
	memo,
	useMemo
} from 'react';
import type {
	ElementType,
	FunctionComponent,
	ReactElement
} from 'react';
import SwipeableViews from 'react-swipeable-views';
import type {
	DrawerProps,
	Orientation,
	TabProps
} from 'src';
import type {
	DrawerHeaderProps
} from 'src/components/_defaults/DrawerHeader';
import DrawerContext from './DrawerContext';
import type {
	DrawerContextProps
} from './DrawerContext';
import './DrawerView.css';

const DrawerView: FunctionComponent<DrawerViewProps> = (
	{
		children: {
			drawers = [],
			tabs = []
		},
		drawerIndex,
		onSelect,
		orientation,
		tabIndex
	}: DrawerViewProps
): ReactElement => {
	const drawerHeader = useMemo(
		(): (DrawerHeader: ElementType<DrawerHeaderProps>) => ReactElement => {
			return (DrawerHeader: ElementType<DrawerHeaderProps>): ReactElement => {
				return (
					<DrawerHeader
						drawerIndex={drawerIndex}
						onDrawerSelect={onSelect}
					>
						{drawers}
					</DrawerHeader>
				);
			};
		},
		[ // eslint-disable-line react-hooks/exhaustive-deps
			drawerIndex,
			onSelect
		]
	);

	const contexts = useMemo(
		(): ReactElement[] => {
			return drawers.map((
				drawer,
				index: number
			): ReactElement<DrawerContextProps> => {
				const className = index === 0 ?
					'content-container-first' :
					// eslint-disable-next-line unicorn/no-nested-ternary
					index === drawers.length - 1 ?
						'content-container-last' :
						'';

				return (
					<DrawerContext
						className={className}
						currentDrawer={index}
						drawer={drawer}
						drawerIndex={drawerIndex}
						key={drawer.props.name}
						orientation={orientation}
						tabIndex={tabIndex}
					>
						{tabs}
					</DrawerContext>
				);
			});
		},
		[ // eslint-disable-line react-hooks/exhaustive-deps
			drawers,
			drawerIndex,
			orientation,
			tabIndex
		]
	);

	return (
		<>
			<UIView
				name='drawerHeader'
				render={drawerHeader}
			/>
			<SwipeableViews
				className='urrd-drawer-swipe'
				disableLazyLoading
				index={
					drawers[drawerIndex] ?
						drawerIndex :
						0
				}
				onChangeIndex={onSelect}
			>
				{contexts}
			</SwipeableViews>
		</>
	);
};

export default memo(
	DrawerView,
	(
		props,
		prevProps // eslint-disable-line unicorn/prevent-abbreviations
	): boolean => {
		if (
			// eslint-disable-next-line lodash/prefer-matches
			props.drawerIndex === prevProps.drawerIndex &&
				props.onSelect === prevProps.onSelect &&
				props.orientation === prevProps.orientation &&
				props.tabIndex === prevProps.tabIndex
		) {
			return true;
		}

		return false;
	}
);

export type DrawerViewProps = {
	children: {
		drawers?: Array<ReactElement<DrawerProps>>;
		tabs?: Array<ReactElement<TabProps>>;
	};
	drawerIndex: number;
	onSelect: (drawerIndex: number) => void;
	orientation: Orientation;
	tabIndex: number;
};

import React, {
	memo,
	useCallback,
	useMemo,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import SwipeableViews from 'react-swipeable-views';
import type {
	Orientation,
	TabProps
} from 'src';
import Content from 'src/components/Content';
import Tab from 'src/components/Tab';
import defaults from 'src/defaults';
import './TabView.css';

const TabView: FunctionComponent<TabViewProps> = (
	{
		children = [],
		onTabSelect = defaults.onTabSelect,
		orientation = defaults.orientation,
		tabIndex = defaults.tabIndex
	}: TabViewProps
): ReactElement => {
	const [
		stateTabIndex,
		setStateTabIndex
	] = useState(0);
	useMemo(
		(): void => {
			setStateTabIndex(tabIndex);
		},
		[tabIndex]
	);

	const views = useMemo(
		(): ReactElement[] => {
			return children.map((
				tab,
				index
			): ReactElement => {
				const className = index === 0 ?
					'content-container-first' :
					// eslint-disable-next-line unicorn/no-nested-ternary
					index === children.length - 1 ?
						'content-container-last' :
						'';

				/* is the left or right sibling hidden? */
				const shadow = !(orientation === 'left' ?
					index < children.length - 1 && children[index + 1].props.hidden :
					index > 0 && children[index - 1].props.hidden);

				return (
					<div
						className='urrd-tab'
						key={tab.props.name}
					>
						<Tab
							view={tab.props.children}
							{...tab.props}
						>
							<Content
								className={shadow ?
									className :
									'content-container-no-shadow'}
								focus={index === tabIndex}
								hidden={tab.props.hidden}
								name={tab.props.name}
								orientation={orientation === 'left' ?
									'right' :
									'left'}
								role='main'
							/>
						</Tab>
					</div>
				);
			});
		},
		[ // eslint-disable-line react-hooks/exhaustive-deps
			children,
			orientation
		]
	);

	const disabled = children[tabIndex] &&
		!children[tabIndex].props.swipe;

	const handleSwipe = useCallback(
		(index: number): void => {
			if (
				children[index] &&
				!children[index].props.hidden
			) {
				onTabSelect(index);
			} else {
				setStateTabIndex(index);
				setTimeout(
					(): void => {
						setStateTabIndex(tabIndex);
					}
				);
			}
		},
		[
			children,
			onTabSelect,
			setStateTabIndex,
			tabIndex
		]
	);

	return (
		<SwipeableViews
			className='urrd-tab-swipe'
			disableLazyLoading
			disabled={disabled}
			index={stateTabIndex}
			onChangeIndex={handleSwipe}
		>
			{views}
		</SwipeableViews>
	);
};

export default memo(
	TabView
);

export type TabViewProps = {
	children?: Array<ReactElement<TabProps>>;
	onTabSelect: (tabIndex: number) => void;
	orientation: Orientation;
	tabIndex: number;
};

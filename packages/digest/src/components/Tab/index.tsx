import {
	useEffect,
	useMemo,
	useRef,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	TabFullProps as TabProps
} from 'src';
import generateStateName from 'src/util/generateStateName';
import initTabViews from 'src/util/initTabViews';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
const Tab: FunctionComponent<TabProps> = (
	{
		children,
		drawers,
		drawerMap,
		footer,
		index,
		name,
		onTabSelect,
		router,
		shortName,
		state,
		tabIndex,
		tabs,
		view
	}: Required<TabProps>
): ReactElement | null => {
	const [
		init,
		setInit
	] = useState(false);

	const reference = useRef<{
		index: number;
		tabIndex: number;
		tabs: Array<ReactElement<TabProps>>;
	}>({
		index,
		tabIndex,
		tabs
	});

	// using ref since props won't be in scope during callback
	reference.current.index = index;
	reference.current.tabIndex = tabIndex;
	reference.current.tabs = tabs;

	useEffect(
		(): () => void => {
			if (
				!router.stateRegistry.get(
					generateStateName(
						shortName,
						name
					)
				)
			) {
				router.stateRegistry.register(
					initTabViews(
						name,
						shortName,
						view as ReactElement,
						footer,
						drawers,
						drawerMap,
						state
					)
				);
			}

			setInit(true);

			return (): void => {
				const reducedTabs = reference.current.tabs.filter(
					(tab): boolean => {
						return name !== tab.props.name;
					}
				);

				if (reference.current.index === reference.current.tabIndex) {
					if (reducedTabs[reference.current.tabIndex + 1]) {
						void router.stateService.go(
							generateStateName(
								shortName,
								reducedTabs[reference.current.tabIndex + 1].props.name
							),
							undefined,
							{
								// eslint-disable-next-line @typescript-eslint/ban-ts-comment
								// @ts-expect-error
								exitSticky: undefined
							}
						);

						onTabSelect(reference.current.tabIndex + 1);
					} else if (reducedTabs[reference.current.tabIndex - 1]) {
						void router.stateService.go(
							generateStateName(
								shortName,
								reducedTabs[reference.current.tabIndex - 1].props.name
							),
							undefined,
							{
								// eslint-disable-next-line @typescript-eslint/ban-ts-comment
								// @ts-expect-error
								exitSticky: undefined
							}
						).then(
							() => {
								// eslint-disable-next-line react-hooks/exhaustive-deps
								onTabSelect(reference.current.tabIndex - 1);
							}
						);
					}
				}

				router.stateRegistry.deregister(
					generateStateName(
						shortName,
						name
					)
				);
			};
		},
		[] // eslint-disable-line react-hooks/exhaustive-deps
	);

	const content = useMemo(
		(): ReactElement => {
			return children as ReactElement;
		},
		[children]
	);

	return init ?
		content :
		null;
};

Tab.defaultProps = {
	swipe: true
};

Tab.displayName = 'Tab';

export default Tab;

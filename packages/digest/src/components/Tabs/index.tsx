import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	TabsProps
} from 'src';

const Tabs: FunctionComponent<TabsProps> = (
	{
		children
	}: TabsProps
): ReactElement => {
	return (
		children as ReactElement
	);
};

Tabs.displayName = 'Tabs';

export default Tabs;

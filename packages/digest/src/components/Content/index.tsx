import {
	UIView
} from '@uirouter/react';
import React, {
	memo,
	useEffect,
	useRef,
	useMemo,
	useState
} from 'react';
import type {
	ComponentType,
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	Orientation
} from 'src';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import styles from './Content.css';

const Content: FunctionComponent<ContentProps> = (
	{
		className,
		focus,
		hidden,
		name,
		orientation,
		role
	}: ContentProps
): ReactElement => {
	const timeout = useRef<NodeJS.Timeout>();
	const [
		stateFocus,
		setStateFocus
	] = useState(focus);

	useEffect(
		(): () => void => {
			if (focus) {
				setStateFocus(focus);
			} else {
				timeout.current = setTimeout(
					(): void => {
						setStateFocus(focus);
					},
					300
				);
			}

			return (): void => {
				if (timeout.current) {
					clearTimeout(timeout.current);
				}
			};
		},
		[focus]
	);

	const Focus = useMemo(
		(): ReactElement | null => {
			return role === 'main' ?
				// eslint-disable-next-line @typescript-eslint/no-extra-parens
				(
					<div
						className='urrd-content-focus'
						id={`urrd_${name}`}
					>
						Main Content
					</div>
				) :
				null;
		},
		[
			role,
			name
		]
	);

	const render = (
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		Component: ComponentType<any>
	): ReactElement => {
		return <Component name={name} />;
	};

	const mainRole = focus && !hidden ?
		role :
		undefined;
	const contentInfoRole = mainRole === 'main' ?
		'contentinfo' :
		undefined;

	return (
		<div
			className={
				orientation === 'left' ?
					styles['content-direction-left'] :
					styles['content-direction-right']
			}
			tabIndex={
				stateFocus ?
					undefined :
					-1
			}
		>
			<div
				aria-hidden={!stateFocus}
				className={
					`${styles['content-container']} ${className}`
				}
				hidden={!stateFocus}
			>
				<div
					className='urrd-content'
					role={mainRole}
				>
					{Focus}
					<UIView
						name={name}
						render={render}
					/>
					<div
						className={
							orientation === 'left' ?
								styles['content-mask-left'] :
								styles['content-mask-right']
						}
						role='presentation'
					/>
				</div>
				<div
					className='urrd-footer-container'
					role={contentInfoRole}
				>
					<UIView name='footer' />
				</div>
			</div>
		</div>
	);
};

export default memo(
	Content,
	(
		props,
		// eslint-disable-next-line unicorn/prevent-abbreviations
		prevProps
	): boolean => {
		if (
			// eslint-disable-next-line lodash/prefer-matches
			props.className === prevProps.className &&
				props.focus === prevProps.focus &&
				props.hidden === prevProps.hidden &&
				props.name === prevProps.name &&
				props.orientation === prevProps.orientation &&
				props.role === prevProps.role
		) {
			return true;
		}

		return false;
	}
);

export type ContentProps = {
	className: string;
	focus: boolean;
	hidden?: boolean;
	name: string;
	orientation: Orientation;
	role?: string;
};

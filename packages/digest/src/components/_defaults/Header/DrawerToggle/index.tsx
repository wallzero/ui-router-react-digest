import React, {
	memo,
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './DrawerToggle.css';

const DrawerToggle: FunctionComponent<DrawerToggleProps> = (
	{
		onToggle
	}: DrawerToggleProps
): ReactElement => {
	const handleClick = useCallback(
		() => {
			onToggle();
		},
		[onToggle]
	);

	return (
		<button
			className='urrd-drawer-toggle-button'
			onClick={handleClick}
			type='button'
		>
			Toggle Drawer
		</button>
	);
};

export default memo(DrawerToggle);

export type DrawerToggleProps = {
	onToggle: () => void;
};

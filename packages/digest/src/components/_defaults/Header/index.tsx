import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	Orientation,
	TabProps
} from 'src';
import defaults from 'src/defaults';
import DrawerToggle from './DrawerToggle';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import styles from './Header.css';
import TabBar from './TabBar';

const Header: FunctionComponent<HeaderProps> = (
	{
		children = [],
		onDrawerOpenToggle = defaults.onDrawerOpenToggle,
		onTabSelect = defaults.onTabSelect,
		orientation = defaults.orientation,
		tabIndex = defaults.tabIndex
	}: HeaderProps
): ReactElement => {
	return (
		<header
			className={
				orientation === 'left' ?
					styles['header-left'] :
					styles['header-right']
			}
		>
			<DrawerToggle
				onToggle={onDrawerOpenToggle}
			/>
			<div
				className={
					orientation === 'left' ?
						styles['header-subheader-left'] :
						styles['header-subheader-right']
				}
			>
				<TabBar
					onTabSelect={onTabSelect}
					orientation={orientation}
					tabIndex={tabIndex}
				>
					{children}
				</TabBar>
			</div>
		</header>
	);
};

export default memo(Header);

export type HeaderProps = {
	children?: Array<ReactElement<TabProps>>;
	drawerOpen?: boolean;
	onDrawerOpenToggle?: () => void;
	onTabSelect?: (index: number) => void;
	orientation?: Orientation;
	tabIndex?: number;
};

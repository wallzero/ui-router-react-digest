import React, {
	memo,
	useCallback,
	useMemo,
	useRef
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	WheelEvent
} from 'react';
import type {
	Orientation,
	TabProps
} from 'src';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import styles from './TabBar.css';

const HORIZONTAL_SCROLL_MINIMUM = 53;

const tabName = (tab: ReactElement<TabProps>): string => {
	return tab.props?.state?.params?.title ?
		tab.props.state.params.title :
		tab.props.name;
};

const TabBar: FunctionComponent<TabBarProps> = (
	{
		children = [],
		onTabSelect,
		orientation = 'left',
		tabIndex
	}: TabBarProps
): ReactElement => {
	const reference = useRef<HTMLSpanElement>(null);

	let filterCount = 0;

	const onClick = (index: number): () => void => {
		return (): void => {
			onTabSelect(index);
		};
	};

	const onScroll = useCallback(
		(
			event: WheelEvent<HTMLDivElement>
		): void => {
			if (reference.current) {
				// Prevent scroll interval less than HORIZONTAL_SCROLL_MINIMUM
				reference.current.scrollLeft += Math.abs(event.deltaY) > HORIZONTAL_SCROLL_MINIMUM ?
					event.deltaY :
					HORIZONTAL_SCROLL_MINIMUM * (event.deltaY < 0 ?
						-1 :
						1);
			}
		},
		[]
	);

	const tabList = useMemo(
		(): ReactElement[] => {
			return children.filter((tab): boolean => {
				if (tab.props.hidden) {
					filterCount += 1; // eslint-disable-line react-hooks/exhaustive-deps

					return false;
				}

				return true;
			}).map((tab, index): ReactElement => {
				const title = tabName(tab);

				return (
					<button
						disabled={index + filterCount === tabIndex}
						key={tab.props.name}
						name={title}
						onClick={onClick(index + filterCount)}
						type='button'
					>
						{title}
					</button>
				);
			});
		},
		[
			children,
			tabIndex,
			onTabSelect
		]
	);

	return (
		<span
			className={
				orientation === 'left' ?
					styles['header-tabs-left'] :
					styles['header-tabs-right']
			}
			onWheel={onScroll}
			ref={reference}
		>
			{tabList}
		</span>
	);
};

export default memo(TabBar);

export type TabBarProps = {
	children?: Array<ReactElement<TabProps>>;
	onTabSelect: (index: number) => void;
	orientation?: Orientation;
	tabIndex: number;
};

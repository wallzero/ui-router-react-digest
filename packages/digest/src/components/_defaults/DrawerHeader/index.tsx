import React, {
	memo,
	useCallback,
	useMemo,
	useRef
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	WheelEvent
} from 'react';
import type {
	DrawerProps
} from 'src';
import defaults from 'src/defaults';
import './DrawerHeader.css';

const HORIZONTAL_SCROLL_MINIMUM = 53;

const drawerName = (drawer: ReactElement<DrawerProps>): string => {
	return drawer.props?.state?.params?.title ?
		drawer.props.state.params.title :
		drawer.props.name;
};

const DrawerHeader: FunctionComponent<DrawerHeaderProps> = (
	{
		children,
		drawerIndex = defaults.drawerIndex,
		onDrawerSelect = defaults.onDrawerSelect
	}: DrawerHeaderProps
): ReactElement => {
	const reference = useRef<HTMLDivElement>(null);

	const onScroll = useCallback(
		(
			event: WheelEvent<HTMLDivElement>
		): void => {
			if (reference.current) {
				// Prevent scroll interval less than 53px
				reference.current.scrollLeft += Math.abs(event.deltaY) > HORIZONTAL_SCROLL_MINIMUM ?
					event.deltaY :
					HORIZONTAL_SCROLL_MINIMUM * (event.deltaY < 0 ?
						-1 :
						1);
			}
		},
		[]
	);

	const drawerList = useMemo(
		(): ReactElement[] | null => {
			const onClick = (index: number): () => void => {
				return (): void => {
					onDrawerSelect(index);
				};
			};

			return children.length > 1 ?
				children.map((
					drawer,
					index
				): ReactElement => {
					const title = drawerName(drawer);

					return (
						<button
							disabled={index === drawerIndex}
							key={drawer.props.name}
							name={title}
							onClick={onClick(index)}
							type='button'
						>
							{title}
						</button>
					);
				}) :
				null;
		},
		[
			children,
			drawerIndex,
			onDrawerSelect
		]
	);

	return (
		<div
			className='urrd-drawer-header'
			onWheel={onScroll}
			ref={reference}
		>
			{drawerList}
		</div>
	);
};

export default memo(DrawerHeader);

export type DrawerHeaderProps = {
	children: Array<ReactElement<DrawerProps>>;
	drawerIndex: number;
	onDrawerSelect: (index: number) => void;
};

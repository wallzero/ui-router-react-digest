import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Footer.css';

const Footer: FunctionComponent<FooterProps> = (): ReactElement => {
	return (
		<div className='urrd-footer'>
			<h2>
				Footer
			</h2>
		</div>
	);
};

export default memo(Footer);

export type FooterProps = {};

import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './DrawerFooter.css';

const DrawerFooter: FunctionComponent<DrawerFooterProps> = (): ReactElement => {
	return (
		<div className='urrd-drawer-footer'>
			<h2>
				Drawer Footer
			</h2>
		</div>
	);
};

export default memo(
	DrawerFooter
);

export type DrawerFooterProps = {};

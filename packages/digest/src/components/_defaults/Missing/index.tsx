import React, {
	memo
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Missing: FunctionComponent = (): ReactElement => {
	return (
		<span>
			<i className='fa fa-times-circle' />
			Missing Template
		</span>
	);
};

export default memo(Missing);

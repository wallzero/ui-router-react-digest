import type {
	FunctionComponent
} from 'react';
import type {
	DrawerProps
} from 'src';

const Drawer: FunctionComponent<DrawerProps> = (): null => {
	return (
		null
	);
};

Drawer.displayName = 'Drawer';

export default Drawer;

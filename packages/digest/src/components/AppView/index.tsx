import {
	UIView
} from '@uirouter/react';
import React, {
	memo,
	useCallback,
	useEffect,
	useMemo,
	useRef
} from 'react';
import type {
	FunctionComponent,
	CSSProperties,
	ElementType,
	KeyboardEvent,
	ReactElement,
	ReactText
} from 'react';
import Sidebar from 'react-sidebar';
import type {
	DrawerProps,
	Orientation,
	TabProps
} from 'src';
import DrawerView from 'src/components/DrawerView';
import type {
	TabViewProps
} from 'src/components/TabView';
import type {
	HeaderProps
} from 'src/components/_defaults/Header';
import defaults from 'src/defaults';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import styles from './AppView.css';

const AppView: FunctionComponent<AppViewProps> = (
	{
		children: {
			drawers = [],
			tabs = []
		},
		drawerDocked = defaults.drawerDocked,
		drawerDrag = defaults.drawerDrag,
		drawerHover = defaults.drawerHover,
		drawerIndex = defaults.drawerIndex,
		drawerOpen = defaults.drawerOpen,
		drawerWidth = defaults.drawerWidth,
		onDrawerOpenToggle = defaults.onDrawerOpenToggle,
		onDrawerSelect = defaults.onDrawerSelect,
		onTabSelect = defaults.onTabSelect,
		orientation = defaults.orientation,
		tabIndex = defaults.tabIndex
	}: AppViewProps
): ReactElement => {
	const reference = useRef<HTMLDivElement>(null);

	/* Remove the role applied by sidebar */
	useEffect(
		(): void => {
			if (
				reference.current?.children?.item(1)
			) {
				// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
				reference.current.children.item(1)!.removeAttribute('role');
			}
		}
	);

	const sidebarStyles: {[key: string]: CSSProperties} = useMemo(
		() => {
			return {
				content: {
					[orientation || 'left']: !drawerHover && drawerOpen ?
						drawerWidth :
						0
				},
				sidebar: {
					width: drawerWidth
				}
			};
		},
		[
			drawerHover,
			drawerOpen,
			drawerWidth,
			orientation
		]
	);

	const onContentSelect = useCallback(
		() => {
			if (
				!drawerDocked &&
					drawerOpen
			) {
				onDrawerOpenToggle();
			}
		},
		[
			drawerDocked,
			drawerOpen,
			onDrawerOpenToggle
		]
	);

	const onContentPress = useCallback(
		(
			event?: KeyboardEvent<Element>
		): void => {
			if (
				!drawerDocked &&
					drawerOpen &&
					event &&
					(event.keyCode === 0 || event.keyCode === 32)
			) {
				onDrawerOpenToggle();
			}
		},
		[
			drawerDocked,
			drawerOpen,
			onDrawerOpenToggle
		]
	);

	const header = useMemo(
		(): (Header: ElementType<HeaderProps>) => ReactElement => {
			return (Header: ElementType<HeaderProps>): ReactElement => {
				return (
					<Header
						drawerOpen={drawerOpen}
						onDrawerOpenToggle={onDrawerOpenToggle}
						onTabSelect={onTabSelect}
						orientation={orientation}
						tabIndex={tabIndex}
					>
						{tabs}
					</Header>
				);
			};
		},
		[
			drawerOpen,
			onDrawerOpenToggle,
			onTabSelect,
			orientation,
			tabIndex,
			tabs
		]
	);

	const sidebar = useMemo(
		(): ReactElement => {
			return (
				<DrawerView
					drawerIndex={drawerIndex}
					onSelect={onDrawerSelect}
					orientation={orientation}
					tabIndex={tabIndex}
				>
					{{
						drawers,
						tabs
					}}
				</DrawerView>
			);
		},
		[
			drawerIndex,
			onDrawerSelect,
			orientation,
			tabIndex,
			drawers,
			tabs
		]
	);

	const content = useMemo(
		(): (TabView: ElementType<TabViewProps>) => ReactElement => {
			return (TabView: ElementType<TabViewProps>): ReactElement => {
				return (
					<TabView
						onTabSelect={onTabSelect}
						orientation={orientation}
						tabIndex={tabIndex}
					>
						{tabs}
					</TabView>
				);
			};
		},
		[
			onTabSelect,
			orientation,
			tabIndex,
			tabs
		]
	);

	return (
		<div
			className={styles.app}
			ref={reference}
		>
			<UIView
				name='header'
				render={header}
			/>
			<Sidebar
				contentClassName={styles[`sidebar-content-${orientation}`]}
				docked={!drawerHover && drawerOpen}
				onSetOpen={onContentSelect}
				open={drawerOpen}
				overlayClassName={styles['sidebar-overlay']}
				pullRight={orientation === 'right'}
				rootClassName={styles['sidebar-root']}
				sidebar={sidebar}
				sidebarClassName={styles.drawers}
				styles={sidebarStyles}
				touch={drawerDrag}
			>
				{/* Only closes the drawer; no role necessary */}
				{/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */}
				<div
					className={styles.main}
					onClick={onContentSelect}
					onKeyPress={onContentPress}
				>
					<UIView
						name='tabs'
						render={content}
					/>
				</div>
			</Sidebar>
		</div>
	);
};

export default memo(
	AppView,
	(
		props,
		prevProps // eslint-disable-line unicorn/prevent-abbreviations
	): boolean => {
		if (
			props.drawerDocked === prevProps.drawerDocked &&
				props.drawerDrag === prevProps.drawerDrag &&
				props.drawerHover === prevProps.drawerHover &&
				props.drawerIndex === prevProps.drawerIndex &&
				props.drawerOpen === prevProps.drawerOpen &&
				props.drawerWidth === prevProps.drawerWidth &&
				props.onDrawerOpenToggle === prevProps.onDrawerOpenToggle &&
				props.onDrawerSelect === prevProps.onDrawerSelect &&
				props.onTabSelect === prevProps.onTabSelect &&
				props.orientation === prevProps.orientation &&
				props.tabIndex === prevProps.tabIndex &&
				props.drawerIndex === prevProps.drawerIndex &&
				props.children.drawers &&
				prevProps.children.drawers &&
				props.children.drawers.length === prevProps.children.drawers.length &&
				props.children.tabs &&
				prevProps.children.tabs &&
				props.children.tabs.length === prevProps.children.tabs.length
		) {
			return true;
		}

		return false;
	}
);

export type AppViewProps = {
	children: {
		drawers?: Array<ReactElement<DrawerProps>>;
		tabs?: Array<ReactElement<TabProps>>;
	};
	drawerDocked: boolean;
	drawerDrag: boolean;
	drawerHover: boolean;
	drawerIndex: number;
	drawerOpen: boolean;
	drawerWidth: ReactText;
	onDrawerOpenToggle: (drawerIndex?: boolean) => void;
	onDrawerSelect: (drawerIndex: number) => void;
	onTabSelect: (tabIndex: number) => void;
	orientation: Orientation;
	tabIndex: number;
};

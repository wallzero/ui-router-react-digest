import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	DrawersProps
} from 'src';

const Drawers: FunctionComponent<DrawersProps> = (
	{
		children
	}: DrawersProps
): ReactElement => {
	return (
		children as ReactElement
	);
};

Drawers.displayName = 'Drawers';

export default Drawers;

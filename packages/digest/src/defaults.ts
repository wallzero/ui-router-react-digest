import type {
	Orientation,
	UIRouterReactDigestProps
} from 'src';

type Defaults = Required<Omit<
UIRouterReactDigestProps,
'drawerFooter' | 'drawerHeader' | 'footer' | 'header' | 'router'
>>;

const defaults: Defaults = {
	children: null,
	drawerDocked: true,
	drawerDrag: false,
	drawerHover: false,
	drawerIndex: 0,
	drawerOpen: false,
	drawerWidth: 300,
	onDrawerOpenToggle: (): void => {},
	onDrawerSelect: (): void => {},
	onTabSelect: (): void => {},
	orientation: 'left' as Orientation,
	rootState: {name: 'uurd'},
	tabIndex: 0
};

export const {
	drawerDocked,
	drawerDrag,
	drawerHover,
	drawerIndex,
	drawerOpen,
	drawerWidth,
	onDrawerOpenToggle,
	onDrawerSelect,
	onTabSelect,
	orientation,
	rootState,
	tabIndex
} = defaults;

export default defaults;

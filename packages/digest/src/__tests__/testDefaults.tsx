/*
	eslint-disable
	jest/no-export
*/
import React, {
	Children
} from 'react';
import type {
	ReactElement
} from 'react';
import Drawer from 'src/components/Drawer';
import Tab from 'src/components/Tab';
import {
	UIRouterReactDigest
} from '..';

describe(
	'App',
	(): void => {
		it(
			'renders without error',
			(): void => {
				// eslint-disable-next-line @typescript-eslint/no-extra-parens
				const app = (
					<UIRouterReactDigest>
						<div>
							test
						</div>
					</UIRouterReactDigest>
				);

				expect(app).toBeDefined();
			}
		);
	}
);

export const drawers = [
	<Drawer
		key={0}
		name='tools'
		state={{
			params: {
				title: 'Tools'
			}
		}}
	/>,
	<Drawer
		key={1}
		name='settings'
		state={{
			params: {
				title: 'Settings'
			}
		}}
	>
		<div>
			<h4>
				Settings
			</h4>
		</div>
	</Drawer>
];

export const drawerMap = Children.toArray(
	drawers
).filter(
	(element): boolean => {
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		return element.type && element.type.displayName === 'Drawer';
	}
) as ReactElement[];

export const tabs = [
	<Tab
		drawers={{
			tools: (): ReactElement => {
				return (
					<div>
						<h4>
							404
						</h4>
					</div>
				);
			}
		}}
		hidden
		key={0}
		name='404'
		state={{
			params: {
				title: '404'
			},

			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-expect-error
			sticky: true
		}}
	>
		<div>
			<h4>
				404
			</h4>
		</div>
	</Tab>,
	<Tab
		drawers={{
			tools: (): ReactElement => {
				return (
					<div>
						<h4>
							Home Tools
						</h4>
					</div>
				);
			}
		}}
		key={1}
		name='home'
		state={{
			params: {
				title: 'Home'
			}
		}}
	>
		<div>
			<h4>
				Home
			</h4>
		</div>
	</Tab>,
	<Tab
		drawers={{
			tools: (): ReactElement => {
				return (
					<div>
						<h4>
							Content Tools
						</h4>
						<div
							style={{
								// stylelint-disable
								height: 1_000
							}}
						/>
					</div>
				);
			}
		}}
		key={2}
		name='content'
		state={{
			params: {
				title: 'Content'
			}
		}}
	>
		<div>
			<h4>
				Content
			</h4>
			<div style={{
				height: 1_000
			}}
			/>
		</div>
	</Tab>
];

export const tabMap = Children.toArray(tabs).filter(
	(element): boolean => {
		// eslint-disable-next-line @typescript-eslint/ban-ts-comment
		// @ts-expect-error
		return element.type && element.type.displayName === 'Tab';
	}
) as ReactElement[];

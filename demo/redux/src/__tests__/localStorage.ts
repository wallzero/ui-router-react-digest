import {
	loadState
} from '../redux/localStorage';

const DATE = Date.now().toString();
const localStorage = {
	getItem: jest.fn().mockReturnValue(JSON.stringify({
		BUILD_DATE: DATE
	}))
};

process.env = {
	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	// @ts-expect-error
	BUILD: {
		DATE
	}
};

describe(
	'Redux state from local storage',
	(): void => {
		describe(
			'Load state',
			(): void => {
				it(
					'undefined local storage',
					(): void => {
						const state = loadState();
						expect(state).toBeUndefined();
					}
				);

				it(
					'error parsing local storage',
					(): void => {
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						jsdom.reconfigure({
							localStorage: {
								...localStorage,
								getItem: jest.fn().mockReturnValue('ewaq{ ]awd{ ]}')
							}
						});

						try {
							loadState();
						} catch (error) {
							// eslint-disable-next-line jest/no-conditional-expect
							expect(error).toBeUndefined();
						} finally {
							expect(true).toBe(true);
						}
					}
				);

				it(
					'empty local storage',
					(): void => {
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						jsdom.reconfigure({
							localStorage
						});

						const state = loadState();
						expect(state).toBeUndefined();
					}
				);

				it(
					'local storage without matching date',
					(): void => {
						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						jsdom.reconfigure({
							localStorage
						});

						process.env = {
							// eslint-disable-next-line @typescript-eslint/ban-ts-comment
							// @ts-expect-error
							BUILD: {
								DATE: '0'
							}
						};

						const state = loadState();
						expect(state).toBeUndefined();
					}
				);
			}
		);
	}
);

import type {
	Orientation
} from 'ui-router-react-digest';

const defaults = {
	drawerDocked: true,
	drawerDrag: false,
	drawerHover: false,
	drawerIndex: 0,
	drawerOpen: true,
	drawers: [{
		name: 'tools',
		state: {
			params: {
				title: 'Tools'
			}
		}
	}, {
		name: 'settings',
		state: {
			params: {
				title: 'Settings'
			}
		}
	}],
	drawerWidth: 300,
	onDrawerOpenToggle: (): void => {},
	onDrawerSelect: (): void => {},
	onTabSelect: (): void => {},
	orientation: 'left' as Orientation,
	rootState: {name: 'uurd'},
	tabIndex: 0,
	tabOrder: [
		'404',
		'home',
		'content'
	],
	tabs: {
		404: {
			hidden: true,
			name: '404',
			state: {
				params: {
					tenured: true,
					title: '404',
					type: '404'
				}
			},
			swipe: false
		},
		content: {
			name: 'content',
			state: {
				deepStateRedirect: true,
				params: {
					swipe: true,
					title: 'Content',
					type: 'content'
				},
				sticky: true
			}
		},
		home: {
			name: 'home',
			state: {
				deepStateRedirect: true,
				params: {
					title: 'Home',
					type: 'home'
				},
				sticky: true
			}
		}
	},
	title: 'Digest'
};

export const {
	drawerDocked,
	drawerDrag,
	drawerHover,
	drawerIndex,
	drawerOpen,
	drawers,
	drawerWidth,
	onDrawerOpenToggle,
	onDrawerSelect,
	onTabSelect,
	orientation,
	rootState,
	tabOrder,
	tabIndex,
	tabs,
	title
} = defaults;

export default defaults;

/*
	eslint-disable
	filenames/match-exported,
	filenames/match-regex,
	@typescript-eslint/prefer-namespace-keyword
*/

import {ReactText} from 'react';
import {
	DrawerProps,
	Orientation,
	TabProps
} from 'ui-router-react-digest';

/* tslint:disable:no-namespace no-internal-module interface-name */
declare module State {
	export type orientation = Orientation;
	export type drawers = DrawerProps[];

	export namespace drawerSettings {
		export type docked = boolean;
		export type drag = boolean;
		export type hover = boolean;
		export type index = number;
		export type open = boolean;
		export type width = ReactText;
	}

	export namespace miscellaneous {
		export type shortName = string;
		export type title = string;
	}

	export type tabOrder = ReactText[];
	export type tab = TabProps;
	export type tabs = {[key: string]: TabProps};

	export namespace tabSettings {
		export type index = number;
	}

	export interface All {
		drawerSettings: {
			docked: drawerSettings.docked;
			drag: drawerSettings.drag;
			hover: drawerSettings.hover;
			index: drawerSettings.index;
			open: drawerSettings.open;
			width: drawerSettings.width;
		};
		drawers: drawers;
		miscellaneous: {
			shortName: miscellaneous.shortName;
			title: miscellaneous.title;
		};
		orientation: orientation;
		tabOrder: tabOrder;
		tabs: tabs;
		tabSettings: {
			index: tabSettings.index;
		};
	}
}

export default State;

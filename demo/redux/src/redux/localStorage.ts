import type State from './state';

declare const process: {
	env: {
		BUILD: {
			DATE: string;
		};
	};
};

type LoadedState = State.All & {
	BUILD_DATE: string;
};

export const loadState = (): Partial<State.All> | undefined => {
	try {
		const serializedState = localStorage.getItem('state');
		if (serializedState === null) {
			return undefined;
		}

		const state = JSON.parse(serializedState) as LoadedState;

		if (
			process.env.BUILD &&
			process.env.BUILD.DATE === state.BUILD_DATE
		) {
			return {
				drawerSettings: state.drawerSettings,
				tabOrder: state.tabOrder,
				tabs: state.tabs,
				tabSettings: state.tabSettings
			};
		}

		return undefined;
	} catch {
		return undefined;
	}
};

export const saveState = (state: State.All): void => {
	try {
		localStorage.setItem(
			'state',
			JSON.stringify({
				BUILD_DATE: process.env.BUILD.DATE,
				drawerSettings: state.drawerSettings,
				tabOrder: state.tabOrder,
				tabs: state.tabs,
				tabSettings: state.tabSettings
			})
		);
	} catch (error) {
		// eslint-disable-next-line no-console
		console.error(error);
	}
};

import type {
	Orientation
} from 'ui-router-react-digest';
import {
	ORIENTATION
} from './ActionTypes';

export type ActionOrientation = {
	position?: Orientation;
	type: typeof ORIENTATION;
};

export const onOrientationToggle = (position?: Orientation): ActionOrientation => {
	return {
		position,
		type: ORIENTATION
	};
};

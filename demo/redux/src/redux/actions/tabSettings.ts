import {
	TAB_SELECT
} from './ActionTypes';

export type ActionTabSelect = {
	tabIndex: number;
	type: typeof TAB_SELECT;
};

export const onTabSelect = (tabIndex: number): ActionTabSelect => {
	return {
		tabIndex,
		type: TAB_SELECT
	};
};

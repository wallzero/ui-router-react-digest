import {
	DRAWER_DOCK,
	DRAWER_DRAG,
	DRAWER_HOVER,
	DRAWER_SELECT,
	DRAWER_OPEN,
	DRAWER_WIDTH
} from '../ActionTypes';
import {
	onDrawerDockedToggle,
	onDrawerDragToggle,
	onDrawerHoverToggle,
	onDrawerOpenToggle,
	onDrawerSelect,
	onDrawerSetWidth
} from '../drawerSettings';
import type {
	ActionDrawerDock,
	ActionDrawerDrag,
	ActionDrawerHover,
	ActionDrawerSelect,
	ActionDrawerOpen,
	ActionDrawerWidth
} from '../drawerSettings';

describe(
	'Drawer settings actions',
	(): void => {
		it(
			'Set docked',
			(): void => {
				const docked = true;
				const expectedAction: ActionDrawerDock = {
					docked,
					type: DRAWER_DOCK
				};

				expect(onDrawerDockedToggle(docked)).toStrictEqual(expectedAction);
			}
		);

		it(
			'Set drag',
			(): void => {
				const drag = true;
				const expectedAction: ActionDrawerDrag = {
					drag,
					type: DRAWER_DRAG
				};

				expect(onDrawerDragToggle(drag)).toStrictEqual(expectedAction);
			}
		);

		it(
			'Set hover',
			(): void => {
				const hover = true;
				const expectedAction: ActionDrawerHover = {
					hover,
					type: DRAWER_HOVER
				};

				expect(onDrawerHoverToggle(hover)).toStrictEqual(expectedAction);
			}
		);

		it(
			'Set open',
			(): void => {
				const open = true;
				const expectedAction: ActionDrawerOpen = {
					open,
					type: DRAWER_OPEN
				};

				expect(onDrawerOpenToggle(open)).toStrictEqual(expectedAction);
			}
		);

		it(
			'Select drawer',
			(): void => {
				const drawerIndex = 2;
				const expectedAction: ActionDrawerSelect = {
					drawerIndex,
					type: DRAWER_SELECT
				};

				expect(onDrawerSelect(drawerIndex)).toStrictEqual(expectedAction);
			}
		);

		it(
			'Set width',
			(): void => {
				const width = 2;
				const expectedAction: ActionDrawerWidth = {
					type: DRAWER_WIDTH,
					width
				};

				expect(onDrawerSetWidth(width)).toStrictEqual(expectedAction);
			}
		);
	}
);

import {
	TAB_SELECT
} from '../ActionTypes';
import {
	onTabSelect
} from '../tabSettings';
import type {
	ActionTabSelect
} from '../tabSettings';

describe(
	'Tab settings actions',
	(): void => {
		it(
			'Select tab',
			(): void => {
				const tabIndex = 2;
				const expectedAction: ActionTabSelect = {
					tabIndex,
					type: TAB_SELECT
				};

				expect(onTabSelect(tabIndex)).toStrictEqual(expectedAction);
			}
		);
	}
);

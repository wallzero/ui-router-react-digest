import {
	TAB_ADD,
	TAB_REMOVE
} from '../ActionTypes';
import {
	onTabAdd,
	onTabRemove
} from '../tabs';
import type {
	ActionTabAdd,
	ActionTabRemove
} from '../tabs';

describe(
	'Tab actions',
	(): void => {
		describe(
			'add',
			(): void => {
				it(
					'Create new tab with random name',
					(): void => {
						const tabIndex = 2;

						const action = onTabAdd(
							undefined,
							tabIndex
						).tab || {name: ''};

						expect(typeof action.name).toBe('string');
					}
				);

				it(
					'Create new tab but don\'t change the index',
					(): void => {
						const tab = {
							name: 'new tab',
							state: {}
						};
						const expectedAction: ActionTabAdd = {
							tab,
							type: TAB_ADD
						};

						expect(
							onTabAdd(tab)
						).toEqual({
							jump: true,
							...expectedAction
						});
					}
				);

				it(
					'Create new tab and go to tab index',
					(): void => {
						const tab = {
							name: 'new tab',
							state: {}
						};
						const tabIndex = 2;
						const expectedAction: ActionTabAdd = {
							jump: true,
							tab,
							tabIndex,
							type: TAB_ADD
						};

						expect(
							onTabAdd(
								tab,
								tabIndex
							)
						).toStrictEqual(expectedAction);
					}
				);

				it(
					'Create new tab and don\'t go to tab index',
					(): void => {
						const tab = {
							name: 'new tab',
							state: {}
						};
						const tabIndex = 2;
						const expectedAction: ActionTabAdd = {
							jump: false,
							tab,
							tabIndex,
							type: TAB_ADD
						};

						expect(
							onTabAdd(
								tab,
								tabIndex,
								false
							)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);

		describe(
			'remove',
			(): void => {
				it(
					'Remove tab',
					(): void => {
						const name = 2;
						const expectedAction: ActionTabRemove = {
							name,
							type: TAB_REMOVE
						};

						expect(
							onTabRemove(name)
						).toStrictEqual(expectedAction);
					}
				);
			}
		);
	}
);

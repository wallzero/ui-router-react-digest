import isNil from 'lodash.isnil';
import isNumber from 'lodash.isnumber';
import {
	DRAWER_DOCK,
	DRAWER_DRAG,
	DRAWER_HOVER,
	DRAWER_OPEN,
	DRAWER_SELECT
} from '../actions/ActionTypes';
import type {
	ActionDrawerDock,
	ActionDrawerDrag,
	ActionDrawerHover,
	ActionDrawerOpen,
	ActionDrawerSelect
} from '../actions/drawerSettings';
import defaultState from '../defaultState';

export default function drawerSettings (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.drawerSettings,
	action: ActionDrawerDock |
	ActionDrawerDrag |
	ActionDrawerHover |
	ActionDrawerOpen |
	ActionDrawerSelect
): DrawerSettings {
	switch (action.type) {
		case DRAWER_DOCK: {
			return {
				...state,
				docked: isNil(action.docked) ?
					!state.docked :
					Boolean(action.docked)
			};
		}

		case DRAWER_DRAG: {
			return {
				...state,
				drag: isNil(action.drag) ?
					!state.drag :
					Boolean(action.drag)
			};
		}

		case DRAWER_HOVER: {
			return {
				...state,
				hover: isNil(action.hover) ?
					!state.hover :
					Boolean(action.hover)
			};
		}

		case DRAWER_OPEN: {
			return {
				...state,
				open: isNil(action.open) ?
					!state.open :
					Boolean(action.open)
			};
		}

		case DRAWER_SELECT: {
			if (isNumber(action.drawerIndex)) {
				return {
					...state,
					index: action.drawerIndex
				};
			}

			return state;
		}

		default: {
			return state;
		}
	}
}

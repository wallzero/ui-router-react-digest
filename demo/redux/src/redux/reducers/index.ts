import {
	combineReducers
} from 'redux';
import drawerSettings from './drawerSettings';
import drawers from './drawers';
import miscellaneous from './miscellaneous';
import orientation from './orientation';
import tabOrder from './tabOrder';
import tabSettings from './tabSettings';
import tabs from './tabs';

const reducers = {
	drawers,
	drawerSettings,
	miscellaneous,
	orientation,
	tabOrder,
	tabs,
	tabSettings
};

export default combineReducers(reducers);

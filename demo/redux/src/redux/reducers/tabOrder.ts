import {
	TAB_ADD,
	TAB_REMOVE
} from '../actions/ActionTypes';
import type {
	ActionTabAdd,
	ActionTabRemove
} from '../actions/tabs';
import defaultState from '../defaultState';
import type State from '../state';

export default (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.tabOrder,
	action: ActionTabAdd |
	ActionTabRemove
): State.tabOrder => {
	switch (action.type) {
		case TAB_ADD: {
			if (action.tab) {
				if (action.tabIndex) {
					return [
						...state.slice(
							0,
							action.tabIndex
						),
						action.tab.name,
						...state.slice(action.tabIndex + 1)
					];
				}

				return [
					...state,
					action.tab.name
				];
			}

			return state;
		}

		case TAB_REMOVE: {
			const tabIndex = state.indexOf(action.name);

			if (state[tabIndex]) {
				return [
					...state.slice(
						0,
						tabIndex
					),
					...state.slice(tabIndex + 1)
				];
			}

			return state;
		}

		default: {
			return state;
		}
	}
};

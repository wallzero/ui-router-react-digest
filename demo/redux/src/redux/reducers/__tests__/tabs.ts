import {
	TAB_ADD,
	TAB_REMOVE
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../tabs';

describe(
	'Tab properties',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.tabs);
			}
		);

		it(
			'adding a tab',
			(): void => {
				const tab = {
					name: 'new tab'
				};
				const complexTab = {
					name: 'new tab',
					state: {
						sticky: false
					}
				};
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabs,
						{
							tab,
							type: TAB_ADD
						}
					)[tab.name]
				).toBeDefined();

				const state = reducer(
					defaultState.tabs,
					{
						tab: complexTab,
						type: TAB_ADD
					}
				)[tab.name].state ?? {sticky: undefined};

				expect(
					state.sticky
				).toStrictEqual(complexTab.state.sticky);

				expect(
					reducer(
						defaultState.tabs,
						{
							tab,
							tabIndex,
							type: TAB_ADD
						}
					)[tab.name]
				).toBeDefined();
			}
		);

		it(
			'removing a tab',
			(): void => {
				const name = 'home';

				expect(
					reducer(
						defaultState.tabs,
						{
							name,
							type: TAB_REMOVE
						}
					)[name]
				).toBeUndefined();

				expect(
					reducer(
						defaultState.tabs,

						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-expect-error
						{
							type: TAB_REMOVE
						}
					)
				).toStrictEqual(defaultState.tabs);
			}
		);
	}
);

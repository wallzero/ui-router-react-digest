import {
	TAB_ADD,
	TAB_SELECT
} from '../../actions/ActionTypes';
import defaultState from '../../defaultState';
import reducer from '../tabSettings';

describe(
	'Tab settings',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(
					reducer(
						undefined,
						// eslint-disable-next-line @typescript-eslint/no-explicit-any
						{} as any
					)
				).toStrictEqual(defaultState.tabSettings);
			}
		);

		it(
			'adding a tab',
			(): void => {
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tab: {name: ''},
							tabIndex,
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabSettings);

				expect(
					reducer(
						defaultState.tabSettings,
						{
							jump: true,
							tab: {name: ''},
							tabIndex,
							type: TAB_ADD
						}
					).index
				).toStrictEqual(tabIndex);

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tab: {name: ''},
							type: TAB_ADD
						}
					)
				).toStrictEqual(defaultState.tabSettings);
			}
		);

		it(
			'selecting a tab',
			(): void => {
				const tabIndex = 1;

				expect(
					reducer(
						defaultState.tabSettings,
						{
							tabIndex,
							type: TAB_SELECT
						}
					).index
				).toStrictEqual(tabIndex);
			}
		);
	}
);

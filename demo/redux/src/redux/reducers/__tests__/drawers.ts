import defaultState from '../../defaultState';
import reducer from '../drawers';

describe(
	'Drawers reducer',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(reducer()).toStrictEqual(defaultState.drawers);
			}
		);
	}
);

import defaultState from '../../defaultState';
import reducer from '../miscellaneous';

describe(
	'Miscellaneous reducer',
	(): void => {
		it(
			'return initial state',
			(): void => {
				expect(reducer()).toStrictEqual(defaultState.miscellaneous);
			}
		);
	}
);

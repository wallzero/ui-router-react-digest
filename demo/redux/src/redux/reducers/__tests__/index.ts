import reducers from '..';

describe(
	'Combined reducers',
	(): void => {
		it(
			'return object containing reducers',
			(): void => {
				expect(reducers).toBeDefined();
			}
		);
	}
);

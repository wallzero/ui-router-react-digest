import {
	TAB_ADD,
	TAB_SELECT
} from '../actions/ActionTypes';
import type {
	ActionTabSelect
} from '../actions/tabSettings';
import type {
	ActionTabAdd,
	ActionTabRemove
} from '../actions/tabs';
import defaultState from '../defaultState';

export default (
	// eslint-disable-next-line @typescript-eslint/default-param-last
	state = defaultState.tabSettings,
	action: ActionTabAdd | ActionTabRemove | ActionTabSelect
): TabSettings => {
	switch (action.type) {
		case TAB_ADD: {
			if (action.jump && action.tabIndex) {
				return {
					...state,
					index: action.tabIndex
				};
			}

			return state;
		}

		case TAB_SELECT: {
			return {
				...state,
				index: action.tabIndex
			};
		}

		default: {
			return state;
		}
	}
};

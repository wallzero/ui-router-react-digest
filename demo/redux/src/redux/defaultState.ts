import {
	drawerDocked,
	drawerDrag,
	drawerHover,
	drawerIndex,
	drawerOpen,
	drawers,
	drawerWidth,
	orientation,
	tabOrder,
	tabIndex,
	tabs,
	title
} from './defaults';
import type State from './state';

const defaultState: State.All = {
	drawers,
	drawerSettings: {
		docked: drawerDocked,
		drag: drawerDrag,
		hover: drawerHover,
		index: drawerIndex,
		open: drawerOpen,
		width: drawerWidth
	},
	miscellaneous: {
		shortName: 'redig',
		title
	},
	orientation,
	tabOrder,
	tabs,
	tabSettings: {
		index: tabIndex
	}
};

export default defaultState;

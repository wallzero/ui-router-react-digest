import type {
	TabProps
} from 'ui-router-react-digest';
import type State from '../state';

const selectTabs = (
	state: Pick<
	State.All,
	'tabOrder' | 'tabs'
	>
): TabProps[] => {
	return state.tabOrder.map(
		(name): TabProps => {
			return state.tabs[name];
		}
	);
};

export {
	selectTabs
};

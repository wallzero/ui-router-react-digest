/*
	eslint-disable
	canonical/filename-match-exported
*/

import React, {
	StrictMode
} from 'react';
import type {
	ReactElement,
	ElementType
} from 'react';
import {
	render
} from 'react-dom';
import {
	hot
} from 'react-hot-loader/root';
import {
	Provider
} from 'react-redux';
import App from 'src/containers/App';
import store from 'src/redux/store';
import './vendor';

if (process.env.NODE_ENV === 'development') {
	/*
	// eslint-disable-next-line promise/prefer-await-to-then
	import('react-dom').then(
		(ReactDOM): void => {
			// @ts-ignore
			return import('react-axe').then( // eslint-disable-line promise/prefer-await-to-then
				// eslint-disable-next-line no-undef,@typescript-eslint/no-explicit-any
				(axe: any): void => {
					return axe(React, ReactDOM, 5000, {
						rules: [{
							enabled: false,
							id: 'region'
						}]
					});
				}
			).catch((): void => {});
		}
	).catch((): void => {});

	/*
	// eslint-disable-next-line no-undef,promise/prefer-await-to-then,@typescript-eslint/no-explicit-any
	import('@welldone-software/why-did-you-render').then((whyDidYouRender: any): void => {
		return whyDidYouRender(
			React,
			{
				collapseGroups: true,
				groupByComponent: true,
				include: [/^Dynamic$/]
			}
		);
	}).catch((): void => {});

	/**/
}

const rootElement = document.querySelector('#app') as HTMLElement ||
	document.createElement('div') as HTMLElement;

const Root: ElementType = hot(
	(): ReactElement => {
		return (
			<Provider
				store={store}
			>
				<App />
			</Provider>
		);
	}
);

render(
	<StrictMode>
		<Root />
	</StrictMode>,
	rootElement
);

export default Root;

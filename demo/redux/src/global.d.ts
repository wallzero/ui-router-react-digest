declare module '*.css';
declare module '*.scss';

declare var System: {
	import: any
};

declare module '*.json' {
	const value: any;
	export default value;
}

declare var BUILD: {
	DATE: string;
};

declare var PACKAGE: {
	DESCRIPTION: string,
	NAME: string,
	TITLE: string,
	VERSION: string
};

interface Miscellaneous {
	shortName: string;
	title: string;
}

interface DrawerSettings {
	docked: boolean;
	drag: boolean;
	hover: boolean;
	index: number;
	open: boolean;
	width: string | number;
}

interface TabSettings {
	index: number;
}


interface PathType {}
interface HookMatchCriterion {}

import {
	DSRPlugin
} from '@uirouter/dsr';
import {
	hashLocationPlugin,

	// pushStateLocationPlugin,
	servicesPlugin,
	UIRouterReact
} from '@uirouter/react';
import {
	StickyStatesPlugin
} from '@uirouter/sticky-states';
import React, {
	memo,
	useEffect,
	useMemo,
	useRef,
	useState
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import {
	Drawer,
	Drawers,
	generateStateName,
	Tab,
	Tabs,
	UIRouterReactDigest
} from 'ui-router-react-digest';
import type {
	DrawerProps,
	Orientation,
	TabProps
} from 'ui-router-react-digest';
import drawerDefs from './drawers';
import tabDefs from './tabs';

// import {Visualizer} from '@uirouter/visualizer';

const rootState = {
	name: 'root',
	params: {
		lang: {
			dynamic: true,
			value: 'en'
		}
	},
	url: '/:lang'
};

const App: FunctionComponent<AppProperties> = (
	{
		drawerDocked,
		drawerDrag,
		drawerHover,
		drawerIndex,
		drawerOpen,
		drawerWidth,
		drawers,
		orientation,
		onDrawerOpenToggle,
		onDrawerSelect,
		onTabSelect,
		tabIndex,
		tabs
	}: AppProperties
): ReactElement | null => {
	const router = useRef<UIRouterReact>();
	const [
		// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
		// @ts-ignore
		_init, // eslint-disable-line @typescript-eslint/no-unused-vars,@typescript-eslint/naming-convention
		setInit
	] = useState<boolean | null>(false);

	useEffect(
		(): void => {
			router.current = new UIRouterReact();

			router.current.plugin(servicesPlugin);
			router.current.plugin(hashLocationPlugin);

			// router.current.plugin(pushStateLocationPlugin);
			router.current.plugin(DSRPlugin);
			router.current.plugin(StickyStatesPlugin);

			// router.plugin(Visualizer);

			// In case of no route found, go to first tab
			router.current.urlService.rules.otherwise(
				(): void => {
					if (router.current) {
						void router.current.stateService.go(
							generateStateName(
								rootState.name,
								'404'
							),
							{},
							{
								location: false,
								reload: true
							}
						);
					}
				}
			);

			router.current.transitionService.onSuccess(
				{
					to: generateStateName(
						rootState.name,
						'**'
					)
				},
				(transition): void => {
					// eslint-disable-next-line unicorn/prevent-abbreviations
					const transitionStateParams = transition.targetState().state().params;

					document.title = transitionStateParams ?
						transitionStateParams.title :
						document.title;
				}
			);

			setInit(null);
		},
		[]
	);

	const tabMap: Array<ReactElement<TabProps>> = useMemo(
		(): ReactElement[] => {
			return tabs.map(
				(tab): ReactElement<TabProps> => {
					if (tab.state?.params) {
						switch (tab.state.params.type) {
							case 'home':
							case 'content':
							case '404': {
								const Component = tabDefs[tab.state.params.type].component;

								return (
									<Tab
										drawers={tabDefs[tab.state.params.type].drawers}
										footer={tabDefs[tab.state.params.type].footer}
										hidden={tab.hidden}
										key={tab.name}
										name={tab.name}
										state={tab.state}
										swipe={tab.swipe}
									>
										<Component />
									</Tab>
								);
							}

							default: {
								const Component = tabDefs.dynamic.component;

								return (
									<Tab
										drawers={tabDefs.dynamic.drawers}
										footer={tabDefs.dynamic.footer}
										hidden={tab.hidden}
										key={tab.name}
										name={tab.name}
										state={tab.state}
										swipe={tab.swipe}
									>
										<Component />
									</Tab>
								);
							}
						}
					} else {
						return (
							<Tab
								hidden={tab.hidden}
								key={tab.name}
								name={tab.name}
								state={tab.state}
								swipe={tab.swipe}
							/>
						);
					}
				}
			);
		},
		[tabs]
	);

	const drawerMap: Array<ReactElement<DrawerProps>> = useMemo(
		(): ReactElement[] => {
			return drawers.map(
				(drawer): ReactElement<DrawerProps> => {
					switch (drawer.name) {
						case 'settings': {
							const Component = drawerDefs[drawer.name].component;

							return (
								<Drawer
									key={drawer.name}
									name={drawer.name}
									state={drawer.state}
								>
									{
										Component ?
											<Component /> :
											null
									}
								</Drawer>
							);
						}

						default: {
							return (
								<Drawer
									key={drawer.name}
									name={drawer.name}
									state={drawer.state}
								/>
							);
						}
					}
				}
			);
		},
		[drawers]
	);

	return router.current ?
		// eslint-disable-next-line @typescript-eslint/no-extra-parens
		(
			<UIRouterReactDigest
				drawerDocked={drawerDocked}
				drawerDrag={drawerDrag}
				drawerHover={drawerHover}
				drawerIndex={drawerIndex}
				drawerOpen={drawerOpen}
				drawerWidth={drawerWidth}
				onDrawerOpenToggle={onDrawerOpenToggle}
				onDrawerSelect={onDrawerSelect}
				onTabSelect={onTabSelect}
				orientation={orientation}
				rootState={rootState}
				router={router.current}
				tabIndex={tabIndex}
			>
				<Drawers>
					{drawerMap}
				</Drawers>
				<Tabs>
					{tabMap}
				</Tabs>
			</UIRouterReactDigest>
		) :
		null;
};

export default memo(App);

export type AppProperties = {
	drawerDocked: boolean;
	drawerDrag: boolean;
	drawerHover: boolean;
	drawerIndex: number;
	drawerOpen: boolean;
	drawerWidth: ReactText;
	drawers: DrawerProps[];
	onDrawerOpenToggle: (toggle?: boolean) => void;
	onDrawerSelect: (index: number) => void;
	onTabSelect: (index: number) => void;
	orientation: Orientation;
	tabIndex: number;
	tabs: TabProps[];
	title: string;
};

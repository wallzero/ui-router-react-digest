/* eslint-disable @typescript-eslint/no-explicit-any */
import type {
	ClassicComponentClass,
	ComponentClass,
	FunctionComponent
} from 'react';
import Content from './Content';
import ContentTools from './Content/Tools';
import Dynamic from './Dynamic';
import DynamicTools from './Dynamic/Tools';
import E404 from './E404';
// eslint-disable-next-line canonical/id-match
import E404Tools from './E404/Tools';
import Home from './Home';
import HomeTools from './Home/Tools';

type TabTypes = {
	[keys: string]: {
		component: ClassicComponentClass<any> | ComponentClass<any> | FunctionComponent<any> ;
		drawers: {
			[keys: string]: ClassicComponentClass<any> | ComponentClass<any> | FunctionComponent<any>;
		};
		footer?: ClassicComponentClass<any> | ComponentClass<any> | FunctionComponent<any>;
	};
};

const tabs: TabTypes = {
	404: {
		component: E404,
		drawers: {
			tools: E404Tools
		}
	},
	content: {
		component: Content,
		drawers: {
			tools: ContentTools
		}
	},
	dynamic: {
		component: Dynamic,
		drawers: {
			tools: DynamicTools
		}
	},
	home: {
		component: Home,
		drawers: {
			tools: HomeTools
		}
	}
};

export default tabs;

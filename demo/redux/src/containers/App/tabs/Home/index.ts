import type {
	UIRouter
} from '@uirouter/core';
import {
	connect
} from 'react-redux';
import connectWithRouter from 'ui-router-react-connect';
import Home from './Home';
import type {
	HomeProps
} from './Home';

const mapRouterToProps = (router: UIRouter): HomeProps => {
	return {
		title: router.stateService.params.title
	};
};

const mapStateToProps = (
	// eslint-disable-next-line no-empty-pattern
	{},
	{title}: {title: string}
): HomeProps => {
	return {
		title
	};
};

export default connectWithRouter(mapRouterToProps)(
	connect(mapStateToProps)(Home)
);

import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Home.css';

const Home: FunctionComponent<HomeProps> = (
	{
		title
	}: HomeProps
): ReactElement => {
	return (
		<div styleName='home'>
			<h2>
				{title}
			</h2>
		</div>
	);
};

export default Home;

export type HomeProps = {
	title: string;
};

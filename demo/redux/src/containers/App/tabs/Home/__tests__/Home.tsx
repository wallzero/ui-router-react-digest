import {
	render
} from '@testing-library/react';
import React from 'react';
import Home from '../Home';

describe(
	'Home tab',
	(): void => {
		const title = 'Home';

		it(
			'title',
			(): void => {
				const component = render(
					<Home title={title} />
				);

				const h2 = component.container.querySelector('h2') ?? {textContent: ''};
				const text = h2.textContent;

				expect(text).toStrictEqual(title);
			}
		);
	}
);

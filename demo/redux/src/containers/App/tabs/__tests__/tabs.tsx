import tabs from '..';

describe(
	'tabs',
	(): void => {
		it(
			'returns object with tab components',
			(): void => {
				expect(tabs).toBeDefined();
			}
		);
	}
);

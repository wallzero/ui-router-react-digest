import React from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText
} from 'react';
import './Dynamic.css';

const Dynamic: FunctionComponent<DynamicProps> = (
	{
		id
	}: DynamicProps
): ReactElement => {
	return (
		<div styleName='tabb'>
			<h2>
				Dynamic Tab
			</h2>
			<h3>
				{id}
			</h3>
		</div>
	);
};

export default Dynamic;

export type DynamicProps = {
	[key: string]: unknown;
	id?: ReactText;
};

import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './Tools.css';

const Tools: FunctionComponent = (): ReactElement => {
	return (
		<div styleName='tools'>
			<h2>
				Dynamic Tools
			</h2>
		</div>
	);
};

export default Tools;

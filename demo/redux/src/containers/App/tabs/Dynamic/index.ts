import type {
	UIRouter
} from '@uirouter/core';
import connectWithRouter from 'ui-router-react-connect';
import Dynamic from './Dynamic';
import type {
	DynamicProps
} from './Dynamic';

type omittedOwnProperties = Record<string, unknown> & {name: string} | undefined;

const mapRouterToProps = (
	router: UIRouter,
	ownProps: omittedOwnProperties
): DynamicProps => {
	return {
		...ownProps,
		id: ownProps?.name
	};
};

export default connectWithRouter(mapRouterToProps)(Dynamic);

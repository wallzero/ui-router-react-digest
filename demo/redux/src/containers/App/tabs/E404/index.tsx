import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import './404.css';

const E404: FunctionComponent = (): ReactElement => {
	return (
		<div styleName='E404'>
			<h2>
				404
			</h2>
		</div>
	);
};

export default E404;

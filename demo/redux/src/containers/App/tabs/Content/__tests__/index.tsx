import {
	render
} from '@testing-library/react';
import {
	memoryLocationPlugin,
	UIRouter,
	UIRouterReact
} from '@uirouter/react';
import React from 'react';
import {
	Provider
} from 'react-redux';
import store from 'src/redux/store';
import Hoc from '..';

describe(
	'Content tab HOC',
	(): void => {
		const title = 'Content';

		it(
			'Custom router',
			(): void => {
				const router = new UIRouterReact();
				router.plugin(memoryLocationPlugin);
				router.stateService.params.title = title;

				const component = render(
					<Provider
						store={store}
					>
						<UIRouter
							router={router}
						>
							<Hoc />
						</UIRouter>
					</Provider>
				);

				const h2 = component.container.querySelector('h2') ?? {textContent: ''};

				const text = h2.textContent;
				expect(text).toStrictEqual(title);
			}
		);
	}
);

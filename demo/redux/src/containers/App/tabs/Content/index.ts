import type {
	UIRouter
} from '@uirouter/core';
import {
	connect
} from 'react-redux';
import connectWithRouter from 'ui-router-react-connect';
import Content from './Content';
import type {
	ContentProps
} from './Content';

const mapRouterToProps = (router: UIRouter): {test: string} => {
	return {
		test: router.stateService.params.title
	};
};

const mapStateToProps = (
	_: Record<string, unknown>,
	{test}: {test: string}
): ContentProps => {
	return {
		title: test
	};
};

export default connectWithRouter(mapRouterToProps)(
	connect(mapStateToProps)(Content)
);

import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaults from 'src/redux/defaults';
import type {
	TabProps
} from 'ui-router-react-digest';
import RemoveTab from '../RemoveTab';

const onTabRemove = jest.fn();

describe(
	'Settings drawer',
	(): void => {
		it(
			'Default properties snapshot',
			(): void => {
				const {
					getByText
				} = render(
					<RemoveTab
						onTabRemove={onTabRemove}
						tabOrder={defaults.tabOrder.map(
							(name): TabProps => {
								return (defaults.tabs as {[key: string]: TabProps})[name];
							}
						)}
					/>
				);

				fireEvent.click(getByText('Remove Tab'));

				expect(onTabRemove).toHaveBeenCalledTimes(1);
			}
		);
	}
);

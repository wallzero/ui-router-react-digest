import {
	connect
} from 'react-redux';
import {
	onTabRemove
} from 'src/redux/actions/tabs';
import {
	selectTabs
} from 'src/redux/selectors/tabs';
import type State from 'src/redux/state';
import RemoveTab from './RemoveTab';
import type {
	RemoveTabProps
} from './RemoveTab';

const mapDispatchToProps = {
	onTabRemove
};

const mapStateToProps = (state: State.All): Omit<RemoveTabProps, keyof typeof mapDispatchToProps> => {
	return {
		tabOrder: selectTabs(state)
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(RemoveTab);

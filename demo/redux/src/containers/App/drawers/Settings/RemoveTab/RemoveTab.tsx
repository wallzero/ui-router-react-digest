import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	ReactText,
	SyntheticEvent
} from 'react';
import type {
	TabProps
} from 'ui-router-react-digest';

const RemoveTab: FunctionComponent<RemoveTabProps> = (
	{
		onTabRemove,
		tabOrder
	}: RemoveTabProps
): ReactElement => {
	const whenTabRemove = useCallback(
		(
			event: SyntheticEvent<HTMLButtonElement>
		): void => {
			event.preventDefault();
			onTabRemove(tabOrder[tabOrder.length - 1].name);
		},
		[
			onTabRemove,
			tabOrder
		]
	);

	return (
		<button
			id='ui-remove-tabs'
			name='Remove Tab'
			onClick={whenTabRemove}
			type='button'
		>
			Remove Tab
		</button>
	);
};

export default RemoveTab;

export type RemoveTabProps = {
	onTabRemove: (name: ReactText) => void;
	tabOrder: TabProps[];
};

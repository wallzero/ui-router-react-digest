import {
	connect
} from 'react-redux';
import {
	onOrientationToggle
} from 'src/redux/actions/orientation';
import type State from 'src/redux/state';
import Orientation from './Orientation';
import type {
	OrientationProps
} from './Orientation';

const mapDispatchToProps = {
	onOrientationToggle
};

const mapStateToProps = (state: State.All): Omit<OrientationProps, keyof typeof mapDispatchToProps> => {
	return {
		orientation: state.orientation
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Orientation);

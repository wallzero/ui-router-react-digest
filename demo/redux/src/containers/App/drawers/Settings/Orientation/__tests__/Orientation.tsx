import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import Orientation from '../Orientation';

describe(
	'Orientation toggle',
	(): void => {
		it(
			'range toggle should call function once',
			(): void => {
				const onOrientationToggle = jest.fn();

				const {
					getByLabelText
				} = render(
					<Orientation
						onOrientationToggle={onOrientationToggle}
						orientation='left'
					/>
				);

				fireEvent.change(
					getByLabelText('Orientation'),
					{
						target: {
							value: 1
						}
					}
				);

				expect(onOrientationToggle).toHaveBeenCalledTimes(1);
			}
		);

		it(
			'range toggle should call function once 2',
			(): void => {
				const onOrientationToggle = jest.fn();

				const {
					getByLabelText
				} = render(
					<Orientation
						onOrientationToggle={onOrientationToggle}
						orientation='right'
					/>
				);

				fireEvent.change(
					getByLabelText('Orientation'),
					{
						target: {
							value: 0
						}
					}
				);

				expect(onOrientationToggle).toHaveBeenCalledTimes(1);
			}
		);
	}
);

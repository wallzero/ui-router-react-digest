import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import type {
	Orientation as TOrientation
} from 'ui-router-react-digest';
import './Orientation.css';

const Orientation: FunctionComponent<OrientationProps> = (
	{
		orientation,
		onOrientationToggle
	}: OrientationProps
): ReactElement => {
	const whenOrientationToggle = useCallback(
		(): void => {
			onOrientationToggle();
		},
		[onOrientationToggle]
	);

	return (
		<label
			htmlFor='ui-orientation'
		>
			<input
				data-testid='ui-orientation'
				id='ui-orientation'
				max='1'
				min='0'
				name='orientation'
				onChange={whenOrientationToggle}
				step='1'
				styleName='range'
				type='range'
				value={orientation === 'right' ?
					1 :
					0}
			/>
			Orientation
		</label>
	);
};

export default Orientation;

export type OrientationProps = {
	onOrientationToggle: (toggle?: TOrientation) => void;
	orientation: TOrientation;
};

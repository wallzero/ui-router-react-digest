import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaults from 'src/redux/defaults';
import Docked from '../Docked';

const onDrawerDockedToggle = jest.fn();

describe(
	'Drawer docked toggle',
	(): void => {
		it(
			'checkbox toggle should call function once',
			(): void => {
				const {
					getByText
				} = render(
					<Docked
						drawerDocked={defaults.drawerDocked}
						onDrawerDockedToggle={onDrawerDockedToggle}
					/>
				);

				fireEvent.click(getByText('Drawer Docked'));

				expect(onDrawerDockedToggle).toHaveBeenCalledTimes(1);
			}
		);
	}
);

import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Docked: FunctionComponent<DockedProps> = (
	{
		drawerDocked,
		onDrawerDockedToggle
	}: DockedProps
): ReactElement => {
	const whenDrawerDockedToggle = useCallback(
		() => {
			onDrawerDockedToggle();
		},
		[onDrawerDockedToggle]
	);

	return (
		<label
			htmlFor='ui-docked'
		>
			<input
				checked={drawerDocked}
				id='ui-docked'
				max='1'
				min='0'
				name='docked'
				onChange={whenDrawerDockedToggle}
				step='1'
				type='checkbox'
			/>
			Drawer Docked
		</label>
	);
};

export default Docked;

export type DockedProps = {
	drawerDocked: boolean;
	onDrawerDockedToggle: (toggle?: boolean) => void;
};

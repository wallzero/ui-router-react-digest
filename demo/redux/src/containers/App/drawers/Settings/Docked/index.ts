import {
	connect
} from 'react-redux';
import {
	onDrawerDockedToggle
} from 'src/redux/actions/drawerSettings';
import type State from 'src/redux/state';
import Docked from './Docked';
import type {
	DockedProps
} from './Docked';

const mapDispatchToProps = {
	onDrawerDockedToggle
};

const mapStateToProps = (state: State.All): Omit<DockedProps, keyof typeof mapDispatchToProps> => {
	return {
		drawerDocked: state.drawerSettings.docked
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Docked);

import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement,
	SyntheticEvent
} from 'react';
import type {
	TabProps
} from 'ui-router-react-digest';

const AddTab: FunctionComponent<AddTabProperties> = (
	{
		onTabAdd,
		tabOrder
	}: AddTabProperties
): ReactElement => {
	const whenTabAdd = useCallback(
		(
			event: SyntheticEvent<HTMLButtonElement>
		): void => {
			event.preventDefault();
			onTabAdd(
				undefined,
				tabOrder.length
			);
		},
		[
			onTabAdd,
			tabOrder.length
		]
	);

	return (
		<button
			id='ui-add-tabs'
			name='Add Tab'
			onClick={whenTabAdd}
			type='button'
		>
			Add Tab
		</button>
	);
};

export default AddTab;

export type AddTabProperties = {
	onTabAdd: (
		tab?: Omit<TabProps, 'name'> & {name?: string},
		tabIndex?: number,
		jump?: boolean
	) => void;
	tabOrder: TabProps[];
};

import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaults from 'src/redux/defaults';
import type {
	TabProps
} from 'ui-router-react-digest';
import AddTab from '../AddTab';

const onTabAdd = jest.fn();

describe(
	'Add tab button',
	(): void => {
		it(
			'button click should call function once',
			(): void => {
				const {
					getByText
				} = render(
					<AddTab
						onTabAdd={onTabAdd}
						tabOrder={defaults.tabOrder.map(
							(name): TabProps => {
								return (defaults.tabs as {[key: string]: TabProps})[name];
							}
						)}
					/>
				);

				fireEvent.click(getByText('Add Tab'));

				expect(onTabAdd).toHaveBeenCalledTimes(1);
			}
		);
	}
);

import {
	connect
} from 'react-redux';
import {
	onTabAdd
} from 'src/redux/actions/tabs';
import {
	selectTabs
} from 'src/redux/selectors/tabs';
import type State from 'src/redux/state';
import AddTab from './AddTab';
import type {
	AddTabProperties
} from './AddTab';

const mapDispatchToProps = {
	onTabAdd
};

const mapStateToProps = (state: State.All): Omit<AddTabProperties, keyof typeof mapDispatchToProps> => {
	return {
		tabOrder: selectTabs(state)
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AddTab);

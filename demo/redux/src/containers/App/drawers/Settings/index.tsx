import React from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';
import AddTab from './AddTab';
import Docked from './Docked';
import Drag from './Drag';
import Hover from './Hover';
import Orientation from './Orientation';
import RemoveTab from './RemoveTab';
import './Settings.css';

const Settings: FunctionComponent = (): ReactElement => {
	return (
		<div styleName='settings'>
			<h2>
				Tabs
			</h2>
			<form>
				<AddTab />
				<br />
				<br />
				<RemoveTab />
			</form>
			<h2>
				Preferences
			</h2>
			<form>
				<Docked />
				<br />
				<Drag />
				<br />
				<Hover />
				<br />
				<Orientation />
			</form>
		</div>
	);
};

export default Settings;

import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaults from 'src/redux/defaults';
import Hover from '../Hover';

const onDrawerHoverToggle = jest.fn();

describe(
	'Drawer hover toggle',
	(): void => {
		it(
			'checkbox toggle should call function once',
			(): void => {
				const {
					getByText
				} = render(
					<Hover
						drawerHover={defaults.drawerHover}
						onDrawerHoverToggle={onDrawerHoverToggle}
					/>
				);

				fireEvent.click(getByText('Drawer Hover'));

				expect(onDrawerHoverToggle).toHaveBeenCalledTimes(1);
			}
		);
	}
);

import {
	connect
} from 'react-redux';
import {
	onDrawerHoverToggle
} from 'src/redux/actions/drawerSettings';
import type State from 'src/redux/state';
import Hover from './Hover';
import type {
	HoverProps
} from './Hover';

const mapDispatchToProps = {
	onDrawerHoverToggle
};

const mapStateToProps = (state: State.All): Omit<HoverProps, keyof typeof mapDispatchToProps> => {
	return {
		drawerHover: state.drawerSettings.hover
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Hover);

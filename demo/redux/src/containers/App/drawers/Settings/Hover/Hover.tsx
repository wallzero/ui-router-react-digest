import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Hover: FunctionComponent<HoverProps> = (
	{
		drawerHover,
		onDrawerHoverToggle
	}: HoverProps
): ReactElement => {
	const whenDrawerHoverToggle = useCallback(
		(): void => {
			onDrawerHoverToggle();
		},
		[onDrawerHoverToggle]
	);

	return (
		<label
			htmlFor='ui-hover'
		>
			<input
				checked={drawerHover}
				id='ui-hover'
				max='1'
				min='0'
				name='hover'
				onChange={whenDrawerHoverToggle}
				step='1'
				type='checkbox'
			/>
			Drawer Hover
		</label>
	);
};

export default Hover;

export type HoverProps = {
	drawerHover: boolean;
	onDrawerHoverToggle: (toggle?: boolean) => void;
};

import {
	fireEvent,
	render
} from '@testing-library/react';
import React from 'react';
import defaults from 'src/redux/defaults';
import Drag from '../Drag';

const onDrawerDragToggle = jest.fn();

describe(
	'Drawer drag toggle',
	(): void => {
		it(
			'checkbox toggle should call function once',
			(): void => {
				const {
					getByText
				} = render(
					<Drag
						drawerDrag={defaults.drawerDrag}
						onDrawerDragToggle={onDrawerDragToggle}
					/>
				);

				fireEvent.click(getByText('Drawer Drag'));

				expect(onDrawerDragToggle).toHaveBeenCalledTimes(1);
			}
		);
	}
);

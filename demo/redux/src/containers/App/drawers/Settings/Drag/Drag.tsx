import React, {
	useCallback
} from 'react';
import type {
	FunctionComponent,
	ReactElement
} from 'react';

const Drag: FunctionComponent<DragProps> = (
	{
		drawerDrag,
		onDrawerDragToggle
	}: DragProps
): ReactElement => {
	const whenDrawerDragToggle = useCallback(
		() => {
			onDrawerDragToggle();
		},
		[onDrawerDragToggle]
	);

	return (
		<label
			htmlFor='ui-drag'
		>
			<input
				checked={drawerDrag}
				id='ui-drag'
				max='1'
				min='0'
				name='drag'
				onChange={whenDrawerDragToggle}
				step='1'
				type='checkbox'
			/>
			Drawer Drag
		</label>
	);
};

export default Drag;

export type DragProps = {
	drawerDrag: boolean;
	onDrawerDragToggle: (toggle?: boolean) => void;
};

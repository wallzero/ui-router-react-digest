import {
	connect
} from 'react-redux';
import {
	onDrawerDragToggle
} from 'src/redux/actions/drawerSettings';
import type State from 'src/redux/state';
import Drag from './Drag';
import type {
	DragProps
} from './Drag';

const mapDispatchToProps = {
	onDrawerDragToggle
};

const mapStateToProps = (state: State.All): Omit<DragProps, keyof typeof mapDispatchToProps> => {
	return {
		drawerDrag: state.drawerSettings.drag
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Drag);

import drawers from '..';

describe(
	'Drawers',
	(): void => {
		it(
			'returns object with drawer components',
			(): void => {
				expect(drawers).toBeDefined();
			}
		);
	}
);

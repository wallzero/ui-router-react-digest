import type {
	ClassicComponentClass,
	ComponentClass,
	FunctionComponent
} from 'react';
import Settings from './Settings';

type Drawers = {
	[keys: string]: {
		component?: ClassicComponentClass | ComponentClass | FunctionComponent;
	};
};

const drawers: Drawers = {
	settings: {
		component: Settings
	}
};

export default drawers;

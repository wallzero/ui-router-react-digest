/*
	eslint-disable
	import/no-commonjs,
	canonical/filename-match-exported,
	canonical/filename-match-regex
 */

// eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports
const path = require('path');
// eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports
const webpackDigest = require('@digest/webpack');

webpackDigest.resolve.alias = {
	'@babel/runtime': path.resolve(
		__dirname,
		'..',
		'..',
		'node_modules',
		'@babel',
		'runtime'
	),
	'react-is': path.resolve(
		__dirname,
		'..',
		'..',
		'node_modules',
		'react-is'
	),
	'react-swipeable-views': path.resolve(
		__dirname,
		'..',
		'..',
		'node_modules',
		'react-swipeable-views'
	),
	'regenerator-runtime': path.resolve(
		__dirname,
		'..',
		'..',
		'node_modules',
		'regenerator-runtime'
	)
};

module.exports = webpackDigest;

/*
	eslint-disable
	filenames/match-regex,
	filenames/match-exported,
	import/unambiguous,
	import/no-commonjs,
	@typescript-eslint/no-var-requires
*/

const jest = require('@digest/jest');

jest.testEnvironment = 'jest-environment-jsdom-global';

module.exports = jest;

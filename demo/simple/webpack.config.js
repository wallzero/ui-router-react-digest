/*
	eslint-disable
	import/unambiguous,
	import/no-commonjs,
	filenames/match-exported,
	filenames/match-regex,
	@typescript-eslint/no-var-requires
*/
const path = require('path');
const webpackDigest = require('@digest/webpack');

webpackDigest.resolve.alias = {
	'@babel/runtime': path.resolve(__dirname, '..', '..', 'node_modules', '@babel', 'runtime'),
	'react-swipeable-views': path.resolve(__dirname, '..', '..', 'node_modules', 'react-swipeable-views'),
	'regenerator-runtime': path.resolve(__dirname, '..', '..', 'node_modules', 'regenerator-runtime')
};

module.exports = webpackDigest;

declare module '*.css';
declare module '*.scss';

declare var System: {
	import: any
};

declare module "*.json" {
		const value: any;
		export default value;
}

declare var BUILD: {
	DATE: string;
}

declare var PACKAGE: {
	DESCRIPTION: string,
	NAME: string,
	TITLE: string,
	VERSION: string
}

import {
	DSRPlugin
} from '@uirouter/dsr';
import {
	hashLocationPlugin,

	// pushStateLocationPlugin,
	servicesPlugin,
	UIRouterReact
} from '@uirouter/react';
import {
	StickyStatesPlugin
} from '@uirouter/sticky-states';
import React, {
	useCallback,
	useEffect,
	useRef,
	useState
} from 'react';
import type {
	CSSProperties,
	FunctionComponent,
	ReactElement
} from 'react';
import {
	Drawer,
	Drawers,
	generateStateName,
	Tab,
	Tabs,
	UIRouterReactDigest
} from 'ui-router-react-digest';
import type {
	Orientation
} from 'ui-router-react-digest';
import 'ui-router-react-digest/dist/main.min.css';

// import {Visualizer} from '@uirouter/visualizer';

const style: CSSProperties = {
	/* stylelint-disable */
	minWidth: 280,
	padding: 20
};

const drawerStyle: CSSProperties = {
	backgroundColor: '#fff',
	display: 'inline-block',
	minHeight: 'calc(100% - 20px)',
	padding: '0 20px 20px',
	width: 'calc(100% - 40px)'
};

const errorStyle: CSSProperties = {
	border: '20px solid black',
	borderImage:
	// eslint-disable-next-line no-multi-str
	'repeating-linear-gradient( \
		45deg, \
		black, \
		black 2px, \
		#fff 2px, \
		#fff 14px, \
		black 14px, \
		black 16px, \
		#fff 16px, \
		#fff 28px \
	) 20 20',
	height: 'calc(100% - 80px)',
	minWidth: 280,
	padding: 20
};

// import {Visualizer} from '@uirouter/visualizer';

const rootState = {
	name: 'root',
	params: {
		lang: {
			dynamic: true,
			value: 'en'
		}
	},
	url: '/:lang'
};

const App: FunctionComponent = (): ReactElement | null => {
	const router = useRef<UIRouterReact>();
	const [
		// eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
		// @ts-ignore
		_init, // eslint-disable-line @typescript-eslint/no-unused-vars,@typescript-eslint/naming-convention
		setInit
	] = useState<boolean | null>(false);

	useEffect(
		(): void => {
			router.current = new UIRouterReact();

			router.current.plugin(servicesPlugin);
			router.current.plugin(hashLocationPlugin);
			router.current.plugin(DSRPlugin);
			router.current.plugin(StickyStatesPlugin);

			// router.plugin(Visualizer);

			// In case of no route found, go to first tab
			router.current.urlService.rules.otherwise(
				(): void => {
					void router.current?.stateService.go(
						generateStateName(
							rootState.name,
							'404'
						),
						{},
						{
							location: false,
							reload: true
						}
					);
				}
			);

			router.current.transitionService.onSuccess(
				{
					to: generateStateName(
						rootState.name,
						'**'
					)
				},
				(transition): void => {
					// eslint-disable-next-line unicorn/prevent-abbreviations
					const transitionStateParams = transition.targetState().state().params;

					document.title = transitionStateParams ?
						transitionStateParams.title :
						document.title;
				}
			);

			/*
			router.current.transitionService.onSuccess(
				{
					to: generateStateName(
						rootState.name,
						'**'
					)
				},
				(transition): void => {
					// ignore, just lazy changing page title
					if (!title) {
						title = document.querySelector('.urrd-header-title');
						if (title) {
							title.innerHTML = 'react-ag';
						}
					}
					document.title = transition.targetState().state().params?.title;
				}
			);
			*/

			setInit(null);
		},
		[]
	);

	const [
		drawerDocked
	] = useState(true);

	const [
		drawerDrag
	] = useState(false);

	const [
		drawerHover
	] = useState(false);

	const [
		drawerIndex,
		setDrawerIndex
	] = useState(0);

	const [
		drawerOpen,
		setDrawerOpen
	] = useState(true);

	// const [
	//   title
	// ] = useState('UIRouter React Digest');

	const [
		orientation
	] = useState('left' as Orientation);

	const [
		tabIndex,
		setTabIndex
	] = useState(0);

	const handleDrawerOpen = useCallback(
		() => {
			setDrawerOpen(!drawerOpen);
		},
		[
			drawerOpen,
			setDrawerOpen
		]
	);

	return router.current ?
		// eslint-disable-next-line @typescript-eslint/no-extra-parens
		(
			<UIRouterReactDigest
				drawerDocked={drawerDocked}
				drawerDrag={drawerDrag}
				drawerHover={drawerHover}
				drawerIndex={drawerIndex}
				drawerOpen={drawerOpen}
				onDrawerOpenToggle={handleDrawerOpen}
				onDrawerSelect={setDrawerIndex}
				onTabSelect={setTabIndex}
				orientation={orientation}
				rootState={rootState}
				router={router.current}
				tabIndex={tabIndex}
			>
				<Drawers>
					<Drawer
						name='tools'
						state={{
							params: {
								title: 'Tools'
							}
						}}
					/>
					<Drawer
						name='settings'
						state={{
							params: {
								title: 'Settings'
							}
						}}
					>
						<div style={drawerStyle}>
							<h4>
								Settings
							</h4>
						</div>
					</Drawer>
				</Drawers>
				<Tabs>
					<Tab
						drawers={{
							// eslint-disable-next-line react/no-unstable-nested-components
							tools: (): ReactElement => {
								return (
									<div style={drawerStyle}>
										<h4>
											404
										</h4>
									</div>
								);
							}
						}}
						hidden
						name='404'
						state={{
							params: {
								title: '404'
							},
							sticky: true
						}}
					>
						<div style={errorStyle}>
							<h4>
								404
							</h4>
						</div>
					</Tab>
					<Tab
						drawers={{
							// eslint-disable-next-line react/no-unstable-nested-components
							tools: (): ReactElement => {
								return (
									<div style={drawerStyle}>
										<h4>
											Home Tools
										</h4>
									</div>
								);
							}
						}}
						name='home'
						state={{
							params: {
								title: 'Home'
							}
						}}
					>
						<div style={style}>
							<h4>
								Home
							</h4>
						</div>
					</Tab>
					<Tab
						drawers={{
							// eslint-disable-next-line react/no-unstable-nested-components
							tools: (): ReactElement => {
								return (
									<div style={drawerStyle}>
										<h4>
											Content Tools
										</h4>
										<div style={{height: 1_000}} />
									</div>
								);
							}
						}}
						name='content'
						state={{
							params: {
								title: 'Content'
							}
						}}
					>
						<div style={drawerStyle}>
							<h4>
								Content
							</h4>
							<div style={{height: 1_000}} />
						</div>
					</Tab>
				</Tabs>
			</UIRouterReactDigest>
		) :
		null;
};

export default App;

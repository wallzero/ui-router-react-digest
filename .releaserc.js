module.exports = {
	fail: false,
	prepare: false,
	publish: [
		{path: '@semantic-release/gitlab'}
	],
	success: false,
	verifyConditions: [
		{path: '@semantic-release/gitlab'}
	]
}

# UI-Router React Digest

| <a href="./stats/ui-router-react-digest/coverage/lcov-report/"><h4>Code coverage</h4></a> | Demo | Stats | Source |
| --- | ---| --- | --- |
| `ui-router-react-digest` | - | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/ui-router-react-digest/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/packages/digest">link</a> |
| `ui-router-react-connect` | - | - | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/packages/connect">link</a></td> |
| Simple Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/simple/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/simple/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/simple">link</a> |
| Redux Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/redux/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/redux/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/redux">link</a> |
| Material Demo | <a href="https://wallzero.gitlab.io/ui-router-react-digest/demo/material/">link</a> | <a href="https://wallzero.gitlab.io/ui-router-react-digest/stats/material/webpack/">link</a> | <a href="https://gitlab.com/wallzero/ui-router-react-digest/tree/master/demo/material">link</a> |
